#! /usr/bin/python
'''
  reads a fastq or fasta, and file splits long reads into shorter, overlapping reads
  
  Usage:
    split_fastq_reads.py -i input.fastq -o output.fastq -l 2047 --min_olap 500 --type fastq
    split_fastq_reads.py -l 2048 --min_olap 500 --type fasta < input.fasta > output.fasta
'''
import sys
import argparse
from .split_paired_end_fastq import iterate_fastq
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
from Bio import SeqIO

def divide_fastq(infile, outfile, length, min_olap):
  if (min_olap >= length):
    raise Exception("overlap length must be shorter than sequence length")
  
  out_num = 1
  for (name, seq, name2, qual) in iterate_fastq:
    while len(seq) > length:
      (seq_chunk, seq) = chunk_seq(seq, length, min_olap)
      (qual_chunk, qual) = chunk_seq(qual, length, min_olap)
      write_fastq_seq(outfile, "seq_"+str(out_num), seq_chunk, qual_chunk)
      out_num += 1
      
    write_fastq_seq(outfile, "seq_"+str(out_num), seq, qual)
    out_num += 1

def divide_fasta(infile, outfile, length, min_olap):
  if (min_olap >= length):
    raise Exception("overlap length must be shorter than sequence length")
  
  out_num = 1
  for seq_obj in SeqIO.parse(infile, 'fasta'):
    seq = str(seq_obj.seq)
    while len(seq) > length:
      (seq_chunk, seq) = chunk_seq(seq, length, min_olap)
      write_fasta_seq(outfile, "seq_"+str(out_num), seq_chunk)
      out_num += 1
      
    write_fasta_seq(outfile, "seq_"+str(out_num), seq)
    out_num += 1

        

def chunk_seq(seq, length, min_olap):
  '''
    input: a sequence, a length of a chunk to return, the minimum amount of 
    sequence overlapping the chunk to leave on the returned sequence
    
    returns: (chunk, sub_seq), where chunk is of length length, and subseq
      is a portion of seq missing some of chunk.
      If len(seq) is <= length, then chunk and sub_seq will both be seq
      Under no other circumstances will sub_seq be shorter than length
    
    Examples:
      chunk_seq("abc", 2, 1) == ("ab", "bc")
      chunk_seq("abcdefghijklm", 5, 3) == ("abcde", "cdefghijklm")
      chunk_seq("abcdefghijklm", 10, 3) == ("abcdefghij", "defghijklm")
      chunk_seq("abcdefghijklm", 500, 3) == ("abcdefghijklm", "abcdefghijklm")
  '''
  if len(seq) <= length:
    return (seq, seq)
  else:
    chunk = seq[0:length]
    if len(seq) - length >= length:
      seq = seq[length-min_olap:]
    else:
      seq = seq[len(seq)-length:]
  return(chunk, seq)
  


def write_fastq_seq(outfile, name, seq, quality):
  outfile.write("@"+name+"\n"+seq+"\n"+"+"+name+"\n"+quality+"\n")
  return

def write_fasta_seq(outfile, name, seq):
  
  rec = SeqRecord(Seq(seq), id=name, description=name)
  
  SeqIO.write(rec, outfile, 'fasta')

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None, help="fastq input")
  parser.add_argument("-o", default=None, help="fastq output")
  parser.add_argument("-l", type=int, default=2047, help="if sequences are longer than this, cut them into sections equal to this")
  parser.add_argument("--min_olap", type=int, default=500, help="when sequences are split into multiple sequences, make sure they overlap by at least this amount")
  parser.add_argument("--type", default="fasta", choices=('fasta', 'fastq'), help="Type of file to input and output.")
  args = parser.parse_args()
  
  if args.i is not None:
    input_handle=open(args.i, "rU")
  else:
    input_handle = sys.stdin
  
  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout
  
  if args.type == "fasta":
    divide_fasta(input_handle, output_handle, args.l, args.min_olap)
  else:
    divide_fastq(input_handle, output_handle, args.l, args.min_olap)
  output_handle.close()
  input_handle.close()
