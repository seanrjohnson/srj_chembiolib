#!/usr/bin/python
'''
  The current version (as of Aug 20, 2017) of the program genemarkS-T has a bug causing it to sometimes output two amino-acid sequence predictions for a transcript, even when only one is requested.
  This program reads the nucleotide and peptide fasta files written by genemarkS-T, and where multiple predictions are found, keeps only the longest.
'''
import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import re

def main(nuc_in, pep_in, nuc_out, pep_out):
  seq_type="fasta"
  genemark_regex=r"(gene_[0-9]+)\|GeneMark.hmm\|([0-9]+)_[a-z]{2}\|[+-]\|[0-9]+\|[0-9]+"
  
  pep_seq_names=dict() #key is sequence ID (everything before the first whitespace), value is a 2-tuple where the first entry is the genemark gene number, and the second entry is the length.
  #nuc_seq_names=dict() #key is sequence ID (everything before the first whitespace), value is a 2-tuple where the first entry is the genemark gene number, and the second entry is the length.
  #pass one, store all sequence definitions, when a new sequence is encountered, if the ID is already in the dicts
  for seq in SeqIO.parse(pep_in, seq_type):
    desc = seq.description
    id = seq.id
    #print(desc)
    #print(id)
    desc_match = re.search(genemark_regex, desc)
    if id not in pep_seq_names:
      pep_seq_names[id] = (desc_match.group(1),int(desc_match.group(2)))
    elif int(desc_match.group(2)) > pep_seq_names[id][1]:
      pep_seq_names[id] = (desc_match.group(1),int(desc_match.group(2)))
  
  #pass two
  pep_in.seek(0)
  for seq in SeqIO.parse(pep_in, seq_type):
    desc = seq.description
    id = seq.id
    desc_match = re.search(genemark_regex, desc)
    if (id in pep_seq_names) and (desc_match.group(1) == pep_seq_names[id][0]):
      SeqIO.write(seq, pep_out, seq_type)
  
  for seq in SeqIO.parse(nuc_in, seq_type):
    desc = seq.description
    id = seq.id
    desc_match = re.search(genemark_regex, desc)
    if (id in pep_seq_names) and (desc_match.group(1) == pep_seq_names[id][0]):
      SeqIO.write(seq, nuc_out, seq_type)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--ni", default=None, required=True, help="nucleotide input fasta")
  parser.add_argument("--pi", default=None, required=True, help="peptide input fasta")
  parser.add_argument("--no", default=None, required=True, help="nucleotide output fasta")
  parser.add_argument("--po", default=None, required=True, help="peptide output fasta")
  
  args = parser.parse_args()
  
  with open(args.ni) as nuc_in:
    with open(args.pi) as pep_in:
      with open(args.no, "w") as nuc_out:
        with open(args.po, "w") as pep_out:
          main(nuc_in, pep_in, nuc_out, pep_out)
          
