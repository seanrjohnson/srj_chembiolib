#! /usr/bin/python
'''

'''
from pyfaidx import Fasta
#from pyfaidx import Faidx
import argparse
import sys


def subset_fasta(input_handle, output_handle, args):
  # fa = Faidx(args.fasta, duplicate_action="first")
  # #fa.build(args.fasta, args.fasta+".fai")
  # for l in input_handle:
  #   l=l.strip()
  #   print(">"+l+"\n"+str(fa[l]),file=output_handle) #TODO: maybe need to use fa.fetch here
  fa = Fasta(args.fasta, duplicate_action="first")
  for l in input_handle:
   l=l.strip()
   print(">"+l+"\n"+str(fa[l]),file=output_handle)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None, help="a list of sequence names to fetch sequences of from the fasta file")
  parser.add_argument("--fasta", default=None, required=True, help="a fasta file containing the full list of sequences to draw the subset from")
  parser.add_argument("-o", default=None)


  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  subset_fasta(input_handle, output_handle, args)

  input_handle.close()
  output_handle.close()