#! /usr/bin/python
'''
  Reads a fasta file and a list of sequence name substrings and strings
  to append to sequence names.
  
  the "position" argument specifies where the new strings should go relative
  to the old strings, and can take the values:
  pre
  post
  replace
  
  
  Usage:
    append_to_seq_name.py -o output.fasta -i input.fasta --position pre --all namestring_1 append_1 namestring_2 append_2 ...
    cat input.fasta | append_to_seq_name.py --subfile file_with_namestring_append_pairs > output.fasta
'''

import sys
import argparse
import fileinput
from Bio import SeqIO


def subset_fasta(input_fasta_handle, names, appends, output_handle, append_all, action, sep):
  for seq in SeqIO.parse(input_fasta_handle, 'fasta'):
      append_list = list()
      if append_all is not None:
        append_list.append(append_all)
      
      for (i, name) in enumerate(names):
        if name in seq.description:
          append_list.append(appends[i])
      to_append = sep.join(append_list)
      if to_append != "":
        if (action == "post"):
          seq.description += sep + to_append
        elif (action == "pre"):
          seq.description = to_append + sep + seq.description
        elif (action == "replace"):
          seq.description = to_append
        seq.id = seq.description
        seq.description=""
        
      
      SeqIO.write(seq, output_handle, 'fasta')



if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("namestring_pairs", nargs='*')
  parser.add_argument("-i", default=None)
  parser.add_argument("-o", default=None)
  parser.add_argument('--position', default=None)
  parser.add_argument("--subfile", default=None, help="name of a file containing lines of tab separated names and annotations")
  parser.add_argument("--all", default=None, help="string to be added to the name of all sequences")
  parser.add_argument("--sep", default=" ", help="string to be used to separate name parts, default is a space")
  
  args = parser.parse_args()
  
  if (args.i is not None):
    input_handle=open(args.i, "rU")
  else:
    input_handle=sys.stdin
  
  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout
  
  input_names = list()
  input_appends = list()
  
  if (args.subfile is not None):
    with open(args.subfile, "rU") as names_input:
      for line in names_input:
        stripped = line.strip()
        if stripped != "":
          splitted = stripped.split("\t")
          input_names.append(splitted[0])
          if len(splitted) > 1:
            input_appends.append(splitted[1])
          else:
            input_appends.append("")
  elif len(args.namestring_pairs) > 0:
    i = 0
    while i < len(args.namestring_pairs):
      if ((i + 1) < len(args.namestring_pairs)):
        input_names.append(args.namestring_pairs[i])
        input_appends.append(args.namestring_pairs[i+1])
      else:
        print("warning: odd number of positional arguments given, final namestring ignored", file=sys.stderr)
  
  action="post"
  if (args.position):
    if (args.position == "pre"):
      action="pre"
    elif (args.position == "replace"):
      action="replace"
  
  subset_fasta(input_handle, input_names, input_appends, output_handle, args.all, action, args.sep)
