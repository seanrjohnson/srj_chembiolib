from Bio import SeqIO
import argparse

def main(args):
  SeqIO.index_db(args.db,args.i, format="fasta")


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  
  parser.add_argument("--db", type=str, default=None, required=True, help="path to sqlite3 database to create")
  parser.add_argument("-i", type=int, default=None, required=True, help="path to fasta file")

  args = parser.parse_args()

  main(args)