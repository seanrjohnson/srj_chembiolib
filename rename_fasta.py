#! /usr/bin/python
'''
  Reads a Fasta file, saves a new fasta file with the same sequences, but altered sequence names
'''

import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import re

def parse_table(infile_name):
  out = dict()
  with open(infile_name, "r") as infile:
    for line in infile:
      parts = line.split("\t")
      out[parts[0].strip()] = parts[1].strip()
  return out

def rename_fasta(input_fasta_handle, output_fasta_handle, args):
  if args.sequencename_table:
    name_table = parse_table(args.sequencename_table)

  substr = None
  if args.substr is not None:
    substr = re.compile(args.substr)

  for seq in SeqIO.parse(input_fasta_handle, args.seq_type):
    desc = seq.description.strip()
    original_desc = desc
    err=None
    iterator = 0

    if not args.replace_old_names:
      out_desc = original_desc
    else:
      out_desc=""
    
    if args.universal_prefix is not None:
      out_desc = args.universal_prefix + out_desc

    if args.sequencename_table:
      if substr:
        match = substr.search(desc)
        if match and len(match.groups()) >= args.substr_group:
          desc = match.group(args.substr_group).strip()
        else:
          err="substring not found %s" % desc
        if not err:
          match = None
          if args.match_style == 'complete':
            if desc in name_table:
              match = name_table[desc]
          else: #match_style == 'substring'
            for k in name_table.keys():
              if k in desc:
                match = name_table[k]
          if (not match) and (args.report_misses):
            print("Name not matched %s" % original_desc, file=sys.stderr)
          if match is not None:
            out_desc = match + out_desc
    if args.append_iterator:
      out_desc = out_desc + str(iterator)
      iterator += 1
    if (err):
      print("err %s" % err, file=sys.stderr)
    else:
      seq.id = out_desc
      seq.description=""
      SeqIO.write(seq, output_handle, args.seq_type)



if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--seq_type", default="fasta", help="")
  parser.add_argument("-i", default=None)
  parser.add_argument("-o", default=None)
  parser.add_argument("--replace_old_names", action='store_true', default=False, help="completely delete the previous sequence name and replace it with the newly generated one")
  parser.add_argument("--universal_prefix", default=None, help="add this prefix to all sequence names")
  parser.add_argument("--substr", default=None, help="a regex to select a part of the fasta definition to look up in the sequencename_table table")

  parser.add_argument("--substr_group", type=int, default=1, help="the capture group from substr to comapare to the table")
  parser.add_argument("--match_style", default='substring', choices=['substring','complete'], help="the capture group from substr to comapare to the table")
  #parser.add_argument("--filename_lookup", default=None, help="a file containing a table with two tab-separated columns, first column has a list of strings to compare to the filename. If one of these strings is found in the filename, the second column will be used as a prefix for all sequence names")
  parser.add_argument("--sequencename_table", default=None, help="a file containing a table with two tab-separated columns, first column has a list of strings to compare to the filename. If one of these strings is found in a sequence name, the second column will be used as a prefix when writing that sequence")
  parser.add_argument("--append_iterator", action='store_true', default=False, help="add an increasing number at the end of the name")
  parser.add_argument("--report_misses", action='store_true', default=False, help="when a sequence name does not have a hit in sequencename_table table, print the sequence name to stderr")



  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  rename_fasta(input_handle, output_handle, args)
