#! /usr/bin/python
'''
  Reads a Fasta file containing open reading frames. Calculates the codon frequency.
  
  Input: a fasta file containing open reading frames.
  
  Output: a tab separated text file where columns are:
    [Amino acid one letter abbreviation] [Codon genetic code] [frequency per thousand]


'''
import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import re
import codon_tables
import pandas as pd

def main(input_fasta_handle, output_handle):
  
  codon_code = codon_tables.eukaryote
  codon_occurence = {x:0 for x in codon_code}
  for seq in SeqIO.parse(input_fasta_handle, "fasta"):
    
    current_codon = ""
    for char in seq.seq:
      if len(current_codon) == 3:
        if current_codon in codon_occurence:
          codon_occurence[current_codon] += 1
        else:
          raise ValueError("Unrecognized codon in sequence %s: %s" % (seq.description, current_codon))
        
        current_codon = ""
      current_codon = current_codon + char
    
    if (len(current_codon) == 3):
      if current_codon in codon_occurence:
        codon_occurence[current_codon] += 1
      else:
        raise ValueError("Unrecognized codon in sequence %s: %s" % (seq.description, current_codon))
    else:
      raise ValueError("Sequence length not divisible by 3: %s" % seq.description)
  
  
  df = pd.DataFrame(columns=("amino_acid", "count", "frequency", "codon"))
  df = df.set_index("codon")
  
  for (k,v) in codon_code.items():
    df.loc[k,"amino_acid"] = v
  
  for (k,v) in codon_occurence.items():
    df.loc[k,"count"] = v
  
  codon_sum = df["count"].sum()
  amino_acid_sums = df.groupby("amino_acid")["count"].sum()
  
  print(amino_acid_sums)
  
  df["frequency"] = df["count"].map(lambda x: round((x/codon_sum)*1000))
  df["relative_frequency"] = df.apply(lambda x: x["count"]/amino_acid_sums[x["amino_acid"]], i)
  
  df.to_csv(output_handle, columns=("amino_acid", "frequency", "relative_frequency"), sep="\t")


def production():
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None, help="if not provided, then input is taken from standard input")
  parser.add_argument("-o", default=None, help="if not provided, then input is taken from standard output")
  
  args = parser.parse_args()
  
  if args.i is not None:
    input_handle=open(args.i, "rU")
  else:
    input_handle = sys.stdin
  
  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout
  
  main(input_handle, output_handle)

def test():
  pass

if __name__ == "__main__":
  production()
