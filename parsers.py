import re
from collections import OrderedDict

def parse_diamond(filename, header=["qseqid", "sseqid", "pident", "length", "mismatch", "gapopen", "qstart", "qend", "sstart", "send", "evalue", "bitscore"]):
	out = list()
	#TODO:need to replace this with an iterator so that the entire file is not stored in memory
	with open(filename) as infile:
		for line in infile:
			line = line.strip()
			rec = OrderedDict()
			parts = line.split("\t",len(header)-1)
			for i,k in enumerate(header):
				rec[k] = parts[i]
			out.append(rec)
	return out

def parse_diamond_iterator(filename, header=["qseqid", "sseqid", "pident", "length", "mismatch", "gapopen", "qstart", "qend", "sstart", "send", "evalue", "bitscore"]):
	out = list()
	with open(filename) as infile:
		for line in infile:
			line = line.strip()
			rec = OrderedDict()
			parts = line.split("\t",len(header)-1)
			for i,k in enumerate(header):
				rec[k] = parts[i]
			yield rec

def parse_fasta(filename):
	"""
		input: the name of a fasta file
		output: a dict where names are sequence id's (id line before first space), and values are the sequence in fasta format (">id length\nsequence")
	"""
	prev_len = 0
	prev_name = None
	prev_seq = ""
	out = dict()
	with open(filename) as input_handle:
		for line in input_handle:
			line = line.strip()
			
			if line[0] == ">":
				parts = line.split(None,1)
				name = parts[0][1:]
				if (prev_name is not None):
					out[prev_name] = ">" + prev_name + " " + str(prev_len) + " \n" + prev_seq
				prev_len = 0
				prev_name = name
				prev_seq = ""
			else:
				prev_len += len(line)
				prev_seq += line
		if (prev_name != None):
			out[prev_name] = ">" + prev_name + " " + str(prev_len) + " \n" + prev_seq
	return out


def parse_clstr(filename):
	cluster_to_members = dict() #probably could be a set of cluster names, not a dict of sets.
	member_to_cluster = dict()
	with open(filename) as infile:
		members = set()
		captain = None
		for line in infile:
			line = line.strip()
			if line[0] == ">":
				if (captain != None) and (len(members) > 0):
					cluster_to_members[captain] = members
					for member in members:
						member_to_cluster[member] = captain
					members = set()
					captain = None
				elif (len(members) > 0):
					print("Error, cannot find ",file=sys.stderr)
			else:
				parts = line.split(None,3)
				name = parts[2][1:-3]
				members.add(name)
				if parts[3] == "*":
					captain = name
		if (captain != None) and (len(members) > 0):
			cluster_to_members[captain] = members
			for member in members:
				member_to_cluster[member] = captain
	return cluster_to_members, member_to_cluster

def _open_if_is_name(filename_or_handle):
	out = filename_or_handle
	input_type = "handle"
	try:
		out = open(filename_or_handle,"r")
		input_type = "name"
	except TypeError:
		pass
	except Exception as e:
		raise(e)

	return (out, input_type)


class hmm_parser:
	def __init__(self, filename, filetype="tblout"):
		if filetype=="domtblout":
			self.header = ["target name", "target accession", "target length", "query name", "query accession", "query length", "full sequence E-value", "full sequence score", "full sequence bias", "this domain #", "this domain of", "this domain c-Evalue", "this domain i-Evalue", "this domain score", "this domain bias", "hmm coord from", "hmm coord to", "ali coord from", "ali coord to", "env coord from", "env coord to", "acc", "description of target"]
		else:
			self.header = ["target name", "target accession", "query name", "query accession", "full sequence E-value", "full sequence score", "full sequence bias", "best domain E-value", "best domain score", "best domain bias", "exp", "reg", "clu", "ov", "env", "dom", "rep", "inc", "description of target"]

		(self.infile, self.input_type) = _open_if_is_name(filename)

	def __iter__(self):
		return self

	def __del__(self):
		if (self.input_type == "name"):
			self.infile.close()

	def __next__(self):
		line = self.infile.readline()
		while (line != ''):
			line = line.strip()
			if ((len(line) == 0) or (line[0] == "#")) :
				line = self.infile.readline()
			else:
				rec = OrderedDict()
				parts = line.split(None,len(self.header)-1)
				for i,k in enumerate(self.header):
					rec[k] = parts[i]
				return rec

		if (self.input_type == "name"):
			self.infile.close()
		raise StopIteration


class hmm_profile_parser:
	#TODO:Test
	def __init__(self, filename):
		self._rec = dict()
		self._state = ""
		(self.infile, self.input_type) = _open_if_is_name(filename)

	def __iter__(self):
		return self

	def __del__(self):
		if (self.input_type == "name"):
			self.infile.close()

	def __next__(self):
		line = self.infile.readline()
		while (line != ''):
			#line = line.strip()
			if (len(line) == 0) :
				pass
			else:
				if (line[0:2] == '//'):
					ret_rec = self._rec
					self._rec = dict()
					self._state=None
					for k,v in ret_rec.items():
						ret_rec[k] = v.strip()
					return ret_rec
				else:
					if (line[0] != ' '):
						parts = line.split(None,1)
						self._state = parts[0]
						if self._state not in self._rec:
							self._rec[self._state] = ""
						if len(parts) > 1:
							self._rec[self._state] += parts[1]
					elif self._state is not None:
						self._rec[self._state] += line
				line = self.infile.readline()
		if (self.input_type == "name"):
			self.infile.close()
		raise StopIteration



class genbank_parser:

	#TODO:I've only tested this on TSA masterrecord files. For genbank files with sequences, you should probably just use the BioPython SeqIO or GenBank parsers.
	def __init__(self, filename):
		(self.infile, self.input_type) = _open_if_is_name(filename)

	def __iter__(self):
		return self

	def add_feature(self,state_l1,state_l2,value,rec):
		if state_l1 is None:
			return
		else:
			if state_l1 not in rec:
				rec[state_l1] = list()
			if state_l2 is None:
				rec[state_l1].append(dict())
				rec[state_l1][-1]["base"] = value
			else:
				rec[state_l1][-1][state_l2] = value

	def parse_feature(self,feature):
		parts = re.split(" +/",feature)
		annots = dict()
		annots['base'] = parts[0]
		for i in range(1,len(parts)):
			(name,val) = parts[i].split('=',1) #split on first '='
			val = val.strip("\"")
			annots[name] = val
		return annots


	def __del__(self):
		if (self.input_type == "name"):
			self.infile.close()

	def __next__(self):
		line = self.infile.readline()
		state_l1 = None
		state_l2 = None
		state_l3 = None
		value = ""
		ret_rec = dict()
		while (line != ''):
			line = line.rstrip()
			if (len(line) == 0) :
				pass
			else:
				if (line[0:2] == '//'):
					self.add_feature(state_l1,state_l2,value,ret_rec)
					return ret_rec
				else:
					field = line[0:12]
					line_value = line[12:]
					c1 = field[0]

					if len(field.strip()) > 0: #this is a new heading
						if state_l1 is not None: #there was a previous heading
							self.add_feature(state_l1,state_l2,value,ret_rec)
						value = line_value
						if c1 != ' ':
							state_l1 = field.strip()
							state_l2 = None
						else:
							state_l2 = field.strip()
					else: #there is no new heading
						value += " " + line_value
			line = self.infile.readline()
		if (self.input_type == "name"):
			self.infile.close()
		raise StopIteration



class a2m:
	def __init__(self,name,seq):
		self.name = name
		self.seq = seq

	def get_segment(self, interval):
		#interval is a tuple (start,end), where start and end are the inclusive, 1-indexed end points of the segment to be extracted.
		#
		i=0
		out = ""
		start = interval[0]
		end = interval[1]
		for c in self.seq:
			if (c == "-" or c.isupper()):
				i += 1
			if ((i >= start) and (i <= end)):
				out += c
		return out

	def get_segment_no_inserts(self, interval):
		#interval is a tuple (start,end), where start and end are the inclusive, 1-indexed end points of the segment to be extracted.
		#
		i=0
		out = ""
		start = interval[0]
		end = interval[1]
		for c in self.seq:
			if (c == "-" or c.isupper()):
				i += 1
				if ((i >= start) and (i <= end)):
					out += c
		return out



class a2m_parser:
	def __init__(self, filename):
		#self._rec = dict()
		#self._state = ""
		self._next_name = None
		(self.infile, self.input_type) = _open_if_is_name(filename)

	def __iter__(self):
		return self

	def __del__(self):
		if (self.input_type == "name"):
			self.infile.close()

	def __next__(self):
		seq = ""
		line = self.infile.readline()
		while (line != ''):
			line = line.strip()
			if (len(line) == 0):
				pass
			else:
				if (line[0] == ">"):
					name = self._next_name
					self._next_name = line[1:]
					if name is not None:
						return a2m(name, seq)
				else:
					seq += line
			line = self.infile.readline()
		if ((self._next_name != None) and (seq != "")):
			return a2m(self._next_name, seq)

		if (self.input_type == "name"):
			self.infile.close()
		raise StopIteration


def parse_hmmer(filename, filetype="tblout"):
	'''
		Returns a list of dicts where each row in the hmm file is an entry in the list.
	'''
	if filetype=="domtblout":
		header = ["target name", "target accession", "target length", "query name", "query accession", "query length", "full sequence E-value", "full sequence score", "full sequence bias", "this domain #", "this domain of", "this domain c-Evalue", "this domain i-Evalue", "this domain score", "this domain bias", "hmm coord from", "hmm coord to", "ali coord from", "ali coord to", "env coord from", "env coord to", "acc", "description of target"]
	else:
		header = ["target name", "target accession", "query name", "query accession", "full sequence E-value", "full sequence score", "full sequence bias", "best domain E-value", "best domain score", "best domain bias", "exp", "reg", "clu", "ov", "env", "dom", "rep", "inc", "description of target"]
	out = list()
	(infile, input_type) = _open_if_is_name(filename)
	for line in infile:
		line = line.strip()
		if line[0] != "#":
			rec = OrderedDict()
			parts = line.split(None,len(header)-1)
			for i,k in enumerate(header):
				rec[k] = parts[i]
			out.append(rec)

	#if a filename was passed, close the file
	if (input_type == "name"):
		infile.close()
	return out

def parse_simple_list(filename):
	"""
		splits a text file on newlines, returns a set with the stripped lines as entries
	"""
	out = set()
	(infile, input_type) = _open_if_is_name(filename)
	
	for l in infile:
		out.add(l.strip())
	
	if (input_type == "name"):
		infile.close()
	return out

class simple_parser:
	def __init__(self, filename):
		'''
			Generator to parse a file line-by-line, stripping lines and returning any that aren't just whitespace.
		'''
		(self.infile, self.input_type) = _open_if_is_name(filename)

	def __iter__(self):
		return self

	def __del__(self):
		if (self.input_type == "name"):
			self.infile.close()

	def __next__(self):
		line = self.infile.readline()
		while (line != ''):
			line = line.strip()
			if (len(line) == 0) :
				line = self.infile.readline()
			else:
				return line

		if (self.input_type == "name"):
			self.infile.close()
		raise StopIteration

