#! /usr/bin/python
'''
  Reads a Fasta file and a list of sequence name substrings, and outputs
  all sequences whose names match the supplied substrings.

  sequence names supplied through stdin should be separated by new lines

  Usage:
    subset_fasta.py -o output.fasta -i input.fasta namestring_1 namestring_2 ...
    cat input.fasta | subset_fasta.py --names namestrings_file > output.fasta

  TODO: Speed up. Two ways to speed up are to make it parallel, and to use SeqIO.index
'''
import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import re

def subset_fasta(input_fasta_handle, names, output_handle, inverse, substring, substring_group, look_for_motif, seq_type, exact):

  if look_for_motif:
    query_seqs = [Seq(x.upper()) for x in names]
    for seq in SeqIO.parse(input_fasta_handle, seq_type):
      for query_seq in query_seqs:
        if (seq.seq.find(query_seq) > -1):
          SeqIO.write(seq, output_handle, seq_type)
          continue
  else:
    substr = None
    if substring is not None:
      substr = re.compile(substring)
    for seq in SeqIO.parse(input_fasta_handle, seq_type):
      keep = True
      desc = seq.description.strip()
      if substr:
        match = substr.search(desc)

        if match and len(match.groups()) >= substring_group:
          desc = match.group(substring_group).strip()
          #print(desc)
        else:
          #print(desc)
          continue
      for name in names:
        name = name.strip()

        if not inverse:
          #print(name)
          if ((not exact) and (name in desc)) or ((exact) and (name == desc)):
            #print(desc)
            SeqIO.write(seq, output_handle, seq_type)
            break
        else:
          if ((not exact) and (name in desc)) or ((exact) and (name == desc)):
            keep = False
      if (inverse and keep):
        SeqIO.write(seq, output_handle, seq_type)


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("namestrings", nargs='*')
  parser.add_argument("--substring", default=None, help="a regular expression to use to extract a substring from the fasta sequence description")
  parser.add_argument("--substring_group", type=int, default=1, help="an integer >= 0 indicating which capture group from the substring regular expression to use")
  parser.add_argument("--seq_type", default="fasta", help="")
  parser.add_argument("-i", default=None)
  parser.add_argument("--names", default=None, help="a text file containing strings to search against the sequence names. One query per line.")
  parser.add_argument("--motif", action='store_true', default=False, help="if set, then interprets the input as a motif to look for in the sequence, rather than in the name. At this time, other options such as substring, and inverse don't work with this option.")
  parser.add_argument("-o", default=None)
  parser.add_argument("--inverse", action='store_true', default=False, help="if set, outputs the sequences where the query is not found, instead of where it is")
  parser.add_argument("--exact", action='store_true', default=False, help="if set, then the input string must match exactly with sequence name")

  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  substring_group = args.substring_group
  if substring_group < 0:
    substring_group = 1

  input_names = list()
  if (args.names is not None):
    with open(args.names, "r") as names_input:
      for line in names_input:
        stripped = line.strip()
        if stripped != "":
          input_names.append(line.rstrip("\r\n"))
  elif len(args.namestrings) > 0:
    input_names = args.namestrings

  subset_fasta(input_handle, input_names, output_handle, args.inverse, args.substring, substring_group, args.motif, args.seq_type, args.exact)
