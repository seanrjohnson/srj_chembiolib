#!/usr/bin/python
'''
  This program reads the nucleotide and peptide fasta files written by transdecoder, and where multiple predictions are found, keeps only the longest. Also renames the sequences to their original name without all the transdecoder cruft.
'''
import argparse
from Bio import SeqIO
import hashlib

def rec_info(rec):
  desc = rec.description.strip()
  id = desc.split("::")[1]
  length = len(rec)
  
  return(desc,id,length)
  

def main(input, output):
  seq_type="fasta"
  
  seqs=dict() #key is sequence name, value is a hash of the sequence string
  
  
  
  for rec in SeqIO.parse(input, seq_type):
    desc = rec.description.strip()
    seq_hash = hashlib.md5(str(rec.seq).encode('utf-8')).hexdigest()
    #print(str(rec.seq))
    #print(str(seq_hash))
    iter = 0
    if desc not in seqs:
      seqs[desc] = seq_hash
      SeqIO.write(rec, output, seq_type)
    elif seq_hash != seqs[desc]:
      print("duplicate! with unique sequence " + desc) 
      while desc + "_" + str(iter) in seqs:
        iter += 1
      #TODO: NOTE that this probably doesn't actually work. you need to change rec.id and rec.description to get the desired effect (what the purpose of rec.name is, I'm not sure).
      rec.description = desc + "_" + str(iter)
      SeqIO.write(rec, output, seq_type)
    else:
      print("duplicate!")

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None, required=True, help="input fasta")
  parser.add_argument("-o", default=None, required=True, help="output fasta")
  
  args = parser.parse_args()
  
  with open(args.i) as input:
    with open(args.o, "w") as output:
      main(input, output)
  input.close()
  output.close()
