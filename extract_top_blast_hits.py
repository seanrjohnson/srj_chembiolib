#!/usr/bin/python
"""
  Reads a blast xml file and outputs the names of the top hits for each
  query sequence
  
  Usage:
    extract_top_blast_hits.py -o output.tsv -i input.xml --evalue 0.001 --num_hits 2 #extract the top two hits for each query of their evalue is better than 0.001
    cat input.xml | extract_top_blast_hits.py --evalue 0.001 --num_hits 2 > output.tsv
"""
import argparse
from Bio.Blast import NCBIXML
import re
import sys

def count_gap_openings(seq):
  '''
    counts gap openings in the sequence
    any "-" preceded by somthing other than a "-" is considered a gap opening
  '''
  return(len(re.findall('[^-]-', seq)))




def main(infile, outfile, num_hits, evalue, swap, rev_file):
  '''
    takes a filehandle for a blast xml file, a handle for an output file
    and specifcations for how many hits to keep and what e-value threshold
    to use.
    
    outputs a tsv file with one hit per line where the first column is the
    query name and the second column is the subject name
  '''

  blast_records = NCBIXML.parse(infile) #use parse, not read, because our input files will probably be quite large
  #outfile.write("query hit subject bits e_value identities query_coverage subject_coverage align_length query_start query_end subject_start subject_end".replace(" ","\t") + "\n")
  #print "started writing"
  for rec in blast_records:
    #NOTE: this assumes that rec.alignments[0].hsps[0] is the best matching hsp
    hits_reported = 0
    for i_align in range(len(rec.alignments)):
      if (hits_reported < num_hits):
        if ((evalue is None) or (rec.alignments[i_align].hsps[0].expect <= float(evalue))):
          hits_reported += 1
          
          if rev_file is not None: #NOTE: This might 
            if rec.alignments[i_align].hsps[0].frame[1] < 0:
              rev_file.write(rec.alignments[i_align].hit_def + "\n")
          
          if (not swap):
            #query
            #use name before any space as the name
            outfile.write(rec.query.split()[0] + "\t") 
          #subject
          outfile.write(rec.alignments[i_align].hit_def)
          if (swap):
            #query
            #use name before any space as the name
            outfile.write("\t" + rec.query.split()[0]) 
          outfile.write("\n")
      else:
        break #go to the next query sequence


if __name__ == '__main__':
  ap = argparse.ArgumentParser()
  ap.add_argument('-i', default=None)
  ap.add_argument('-o', default=None)
  ap.add_argument('--rev_file', default=None, help="filename for a file to save the names of subject sequences that matched in a reverse frame")
  ap.add_argument('--num_hits', type=int, default=1)
  ap.add_argument('--evalue', type=float, default=None)
  ap.add_argument('--swap_output', action='store_true', default=False, help="if active, print the name of the hit before the name of the query in the output")
  args = ap.parse_args()
  
  infile = sys.stdin
  outfile = sys.stdout
  
  if args.i is not None:
    infile = open(args.i, 'rb')
  if args.o is not None:
    outfile = open(args.o, 'w')
  
  rev_file = None
  if args.rev_file is not None:
    rev_file = open(args.rev_file, 'w')
    
  main(infile, outfile, args.num_hits, args.evalue, args.swap_output, rev_file)
  infile.close()
  outfile.close()
  if rev_file is not None:
    rev_file.close()
