#! /usr/bin/python
'''
  Reads a fasta file and a list of sequence name substrings
  The input fasta is output, but with the reverse complement of any sequence
  whose name matches something in the list of name substrings
  
  
  Usage:
    reverse_complement_fasta.py -o output.fasta -i input.fasta namestring_1 namestring_2 ...
    reverse_complement_fasta.py --all -o output.fasta -i input.fasta
    cat input.fasta | reverse_complement_fasta.py --names file_with_namestrings > output.fasta
'''

import sys
import argparse
import fileinput
from Bio import SeqIO


def revc_fasta(input_fasta_handle, names, output_handle, reverse_all):
  for seq in SeqIO.parse(input_fasta_handle, 'fasta'):
      rev = reverse_all
      if (not reverse_all):
        for (i, name) in enumerate(names):
          if name in seq.description:
            rev = True
            break

      if rev == True:
        seq.seq = seq.seq.reverse_complement()
      
      SeqIO.write(seq, output_handle, 'fasta')



if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("namestrings", nargs='*', help="A list of sequence name substrings of sequences to return the reverse complement of")
  parser.add_argument("-i", default=None, help="fasta input")
  parser.add_argument("-o", default=None, help="fasta output")
  parser.add_argument("--names", default=None, help="name of a file containing substrings to search for in the sequence names, separated by newlines")
  parser.add_argument("--all", action='store_true', default=False, help="If set, return the reverse complements of all sequences in the input")
  
  args = parser.parse_args()
  
  if args.i is not None:
    input_handle=open(args.i, "rU")
  else:
    input_handle = sys.stdin
  
  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout
  
  input_names = list()
  if (args.names is not None):
    with open(args.names, "rU") as names_input:
      for line in names_input:
        stripped = line.strip()
        if stripped != "":
          input_names.append(stripped)
  elif len(args.namestrings) > 0:
    input_names = args.namestrings
  
  revc_fasta(input_handle, input_names, output_handle, args.all)
