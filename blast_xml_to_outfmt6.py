#! /usr/bin/python
'''
  converts Blast xml output to outfmt6
  
  heading format for output:
  query	subject	percent_identity	alignment_length	mismatches	gap_opens	query_start	query_end	subject_start	subject_end	e-value	bit_score

  usage: 
  blast_xml_to_outfmt6.py -i input.xml -o output.outfmt6 --num_hits 1 --num_hsps 1 --evalue 1e-10
'''

import argparse
from Bio.Blast import NCBIXML
import re
import sys

def count_gap_openings(seq):
  '''
    counts gap openings in the sequence
    any "-" preceded by somthing other than a "-" is considered a gap opening
  '''
  return(len(re.findall('[^-]-', seq)))

def seq_types(application):
  if application == "BLASTP":
    query_type = "prot"
    target_type = "prot"
    align_type = "prot"
  elif application == "BLASTN":
    query_type = "nuc"
    target_type = "nuc"
    align_type = "nuc"
  elif application == "BLASTX":
    query_type = "nuc"
    target_type = "prot"
    align_type = "prot"
  elif application == "TBLASTN":
    query_type = "prot"
    target_type = "nuc"
    align_type = "prot"
  
  return(query_type, target_type, align_type)


def main(infile, outfile, num_hits, num_hsps, evalue, use_fillers, write_header, identity_cutoff, similarity_cutoff, target_def, full_target_name, full_query_name, extended_table, target_name_and_description):
  '''
    The output of this function is intended to be the same as it would be from blast with the options
    -max_target_seqs num_hits -evalue evalue -outfmt 6
  '''

  blast_records = NCBIXML.parse(infile) #use parse, not read, because our input files will probably be quite large
  
  if write_header:
    out_line="query subject percent_identity alignment_length mismatches gap_opens query_start query_end subject_start subject_end e-value bit_score"
    if (extended_table):
      out_line = out_line + " subject_length query_length length_type"
    outfile.write(out_line.replace(" ","\t") + "\n")
  #print "started writing"
  for rec in blast_records:
    #NOTE: this assumes that rec.alignments[0].hsps[0] is the best matching hsp
    hits_reported = 0
    total_hsps_reported = 0
    
    application = rec.application
    
    if extended_table:
      if application not in ('BLASTP', 'BLASTN', 'BLASTX', 'TBLASTN'):
        raise ValueError("application %s extended table support not implemented" % application)
      (query_type, subject_type, align_type) = seq_types(application)
    
    
    
    for i_align in range(len(rec.alignments)):
      if ((num_hits is None) or (hits_reported < num_hits)):
        if ((evalue is None) or (rec.alignments[i_align].hsps[0].expect <= float(evalue))):
          hits_reported += 1
          hsps_reported = 0
          for hsp in range(len(rec.alignments[i_align].hsps)):
            #the gauntlet of thresholds
            if ( ((evalue is None) or rec.alignments[i_align].hsps[hsp].expect <= float(evalue)) and ((num_hsps is None) or (hsps_reported < num_hsps)) ):
              if (similarity_cutoff is None) or (100 * rec.alignments[i_align].hsps[hsp].positives/float(rec.query_length) >= similarity_cutoff):
                if (identity_cutoff is None) or (100 * rec.alignments[i_align].hsps[hsp].identities/float(rec.query_length) >= identity_cutoff):
                  
                  if full_query_name:
                    query_name = rec.query.strip() #use the full qyery def
                  else:
                    query_name = rec.query.split()[0] #use name before any space as the name
                  if target_def:
                    subject_name = rec.alignments[i_align].hit_def
                  elif target_name_and_description:
                    subject_name = rec.alignments[i_align].hit_id + " " + rec.alignments[i_align].hit_def
                  else:
                    subject_name = rec.alignments[i_align].hit_id
                  
                  if not full_target_name:
                    subject_name = subject_name.split()[0]
                  
                  if extended_table:
                    subject_length = rec.alignments[i_align].length
                    query_length = rec.query_length
                    if subject_type == "nuc" and align_type == "prot":
                      subject_length = subject_length//3
                    if query_type == "nuc" and align_type == "prot":
                      query_length = query_length//3
                  
                  percent_identity = "%.2f" % (100 * float(rec.alignments[i_align].hsps[hsp].identities)/float(rec.alignments[i_align].hsps[hsp].align_length))
                  length = str(rec.alignments[i_align].hsps[hsp].align_length)
                  mismatch = str(rec.alignments[i_align].hsps[hsp].align_length-(rec.alignments[i_align].hsps[hsp].identities+rec.alignments[i_align].hsps[hsp].gaps))
                  gapopen = str(count_gap_openings(rec.alignments[i_align].hsps[hsp].match) + count_gap_openings(rec.alignments[i_align].hsps[hsp].sbjct))
                  query_start = str(rec.alignments[i_align].hsps[hsp].query_start)
                  query_end = str(rec.alignments[i_align].hsps[hsp].query_end)
                  subject_start = str(rec.alignments[i_align].hsps[hsp].sbjct_start)
                  subject_end = str(rec.alignments[i_align].hsps[hsp].sbjct_end)
                  e_value = str(rec.alignments[i_align].hsps[hsp].expect)
                  bits = str(rec.alignments[i_align].hsps[hsp].bits)
                  hsps_reported += 1
                  total_hsps_reported += 1

                  outfile.write(query_name)
                  outfile.write("\t"+subject_name)
                  outfile.write("\t"+percent_identity)
                  outfile.write("\t"+length)
                  outfile.write("\t"+mismatch)
                  outfile.write("\t"+gapopen)
                  outfile.write("\t"+query_start)
                  outfile.write("\t"+query_end)
                  outfile.write("\t"+subject_start)
                  outfile.write("\t"+subject_end)
                  outfile.write("\t"+e_value)
                  outfile.write("\t"+bits)
                  if (extended_table):
                    outfile.write("\t"+str(subject_length))
                    outfile.write("\t"+str(query_length))
                    outfile.write("\t"+str(align_type))
                  outfile.write("\n")
      else:
        break #go to the next query sequence
    if (use_fillers and (total_hsps_reported == 0) ):
      outfile.write(rec.query.split()[0] + "\n")


if __name__ == '__main__':
  ap = argparse.ArgumentParser()
  ap.add_argument('-i', default=None)
  ap.add_argument('-o', default=None)
  ap.add_argument('--num_hits', type=int, default=None, help="maximum number of subject sequences to report for each query sequence. Default: all of them")
  ap.add_argument('--num_hsps', type=int, default=None, help="maximum number of HSPs to report for each reported database hit. Default: all of them")
  ap.add_argument('--target_identity', type=float, default=None, help="Miniumum 100 * Hsp_identity / query_length cutoff for reporting a hit. Default: None")
  ap.add_argument('--target_similarity', type=float, default=None, help="Miniumum 100 * Hsp_positive / query_length cutoff for reporting a hit. Default: None")
  ap.add_argument('--evalue', type=float, default=None, help="e-value cutoff for reporting a hit. Default: None")
  ap.add_argument('--use_fillers', action='store_true', default=False, help="If set, include in output the query names for queries that had no hits")
  ap.add_argument("--write_header", action='store_true', default=False, help="If set, writes a header line as the first line of the output file")
  ap.add_argument("--target_def", action='store_true', default=False, help="If set, take target names from the 'hit_def' field instead of the 'hit_id' field")
  ap.add_argument("--full_target_name", action='store_true', default=False, help="If set, use the full field of the target name, otherwise use everything before the first space")
  ap.add_argument("--full_query_name", action='store_true', default=False, help="If set, use the full field of the query name, otherwise use everything before the first space")
  ap.add_argument("--target_name_and_description", action='store_true', default=False, help="If set, use the target id and description.")
  ap.add_argument("--extended_table", action='store_true', default=False, help="If set, add more fields to the output.")
  
  #ap.add_argument("--HSP_sum", action='store_true', default=False, help="If set, combine hsp bit scores and stats") #TODO: this will be a bit complicated...
  args = ap.parse_args()
  
  infile = sys.stdin
  outfile = sys.stdout
  
  if args.i is not None:
    infile = open(args.i, 'r')
  if args.o is not None:
    outfile = open(args.o, 'w')
  
  full_target_name = args.full_target_name
  if args.target_name_and_description:
      full_target_name = True

  main(infile, outfile, args.num_hits, args.num_hsps, args.evalue, args.use_fillers, args.write_header, args.target_identity, args.target_similarity, args.target_def, full_target_name, args.full_query_name, args.extended_table, args.target_name_and_description)
  infile.close()
  outfile.close()
