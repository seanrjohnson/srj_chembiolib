#!/usr/bin/python
"""
  Reads two sequence files, a 'target' file and a 'query' file. Finds the closest homologs of 'query' in 'target'.
  Each sequence in target is assigned to at most one sequence in query.
  For example, if query is a set of protein sequences, and target is a set of RNAseq contigs, it will find transcripts
  encoding homologs of the query proteins.
  Programs from BLAST+ must be in the PATH for this script to work
  
  Output is a tab separated text file in BLAST+ outfmt6 format with a header.
"""
#TODO: some of the code in this script is basically copy pasted from blast_xml_to_outfmt6.py. It might be good to consolidate
import sys
import os
import subprocess
import argparse
from Bio.Blast import NCBIXML
import re

def generate_title_line(blast_type):
  title_line = "query target percent_identity alignment_length mismatches gap_opens query_start query_end target_start target_end e-value bit_score"
  if blast_type == "blastn":
    title_line += " query_strand"
    title_line += " target_strand"
  elif blast_type == "blastp":
    pass
  elif blast_type == "blastx":
    title_line += " query_frame"
  elif blast_type == "tblastn":
    title_line += " target_frame"
    
  return title_line.replace(" ","\t")

def main(outfile, args):
  
  #make the target database
  target_basename = os.path.basename(args.t)
  target_database = os.path.join(args.temp_dir, target_basename)
  parse_seqids = ""
  if not args.skip_makedb:
    if args.parse_seqids:
      subprocess.call(["makeblastdb", "-hash_index","-parse_seqids","-dbtype", args.target_type, "-in", args.t, "-out", target_database])
    else:
      subprocess.call(["makeblastdb", "-hash_index","-dbtype", args.target_type, "-in", args.t, "-out", target_database])
  
  program = "blastn"
  word_size = 3
  if args.target_type == "prot":
    if args.query_type == "prot":
      program = "blastp"
      
      if args.word_size is not None:
        word_size = args.word_size
      else:
        word_size = 3
    elif args.query_type == "nucl":
      program = "blastx"
      
      if args.word_size is not None:
        word_size = args.word_size
      else:
        word_size = 3
  elif args.target_type == "nucl":
    if args.query_type == "prot":
      program = "tblastn"
          
      if args.word_size is not None:
        word_size = args.word_size
      else:
        word_size = 7
    elif args.query_type == "nucl":
      program = "blastn"
      
      if args.word_size is not None:
        word_size = args.word_size
      else:
        word_size = 7
  
  num_threads = args.num_threads
  if num_threads <= 0 :
    num_threads = 1
  
  blast_output = os.path.join(args.temp_dir, os.path.basename(args.q) + "_" + target_basename + ".xml")
  if not args.skip_blast:
    subprocess.call([program, "-db", target_database, "-word_size", str(word_size), "-num_threads", str(num_threads) ,"-evalue", str(args.evalue), "-query", args.q, "-out", blast_output, "-outfmt", str(5)])
  
  
  target_matches = dict() #dict of lists of recs
  with open(blast_output, "r") as blast_xml_input:
    blast_records = NCBIXML.parse(blast_xml_input)
    
    #find matches that fit the filter criteria and sort them according to the database sequence
    for rec in blast_records:
      #NOTE: this assumes that rec.alignments[0].hsps[0] is the best matching hsp
      for i_align in range(len(rec.alignments)):
        percent_identity = 100 * float(rec.alignments[i_align].hsps[0].identities)/float(rec.query_length)
        percent_similarity = 100 * float(rec.alignments[i_align].hsps[0].positives)/float(rec.query_length)
        #print(percent_similarity)
        if ((args.evalue is None) or (rec.alignments[i_align].hsps[0].expect <= args.evalue)):
          if ((args.bitscore is None) or (rec.alignments[i_align].hsps[0].score >= args.bitscore)):
            if ((args.ident is None) or (percent_identity >= args.ident)):
              if ((args.sim is None) or (percent_similarity >= args.sim)):
                target_name = rec.alignments[i_align].hit_def
                if target_name not in target_matches:
                  target_matches[target_name] = list()
                target_matches[target_name].append({'query': rec.query, 'alignment': rec.alignments[i_align]})
    
  #sort matches by e-value (this may not be the best criterion, I don't know)
  for targ in target_matches:
    target_matches[targ] = sorted(target_matches[targ], key=lambda x: x['alignment'].hsps[0].expect)
  
  #write output:
  title_line = generate_title_line(program)
  outfile.write(title_line + "\n")
  for targ in target_matches:
    query_name = target_matches[targ][0]['query']
    hsp = target_matches[targ][0]['alignment'].hsps[0]
    
    percent_identity = "%.2f" % (100 * float(hsp.identities)/float(hsp.align_length))
    length = str(hsp.align_length)
    mismatch = str(hsp.align_length-(hsp.identities+hsp.gaps))
    gapopen = str(count_gap_openings(hsp.match) + count_gap_openings(hsp.sbjct))
    query_start = str(hsp.query_start)
    query_end = str(hsp.query_end)
    subject_start = str(hsp.sbjct_start)
    subject_end = str(hsp.sbjct_end)
    e_value = str(hsp.expect)
    bits = str(hsp.bits)
    
    outfile.write(query_name)
    outfile.write("\t"+targ)
    outfile.write("\t"+percent_identity)
    outfile.write("\t"+length)
    outfile.write("\t"+mismatch)
    outfile.write("\t"+gapopen)
    outfile.write("\t"+query_start)
    outfile.write("\t"+query_end)
    outfile.write("\t"+subject_start)
    outfile.write("\t"+subject_end)
    outfile.write("\t"+e_value)
    outfile.write("\t"+bits)
    #TODO: test this for all of the BLAST types (currently only tested for BLASTN)
    if program == "blastn":
      #~ print("strand")
      #~ print(hsp.strand)
      #~ print("frame")
      #~ print(hsp.frame)
      outfile.write("\t"+ str(hsp.frame[0]))
      outfile.write("\t"+ str(hsp.frame[1]))
    elif program == "blastp":
      pass
    elif program == "blastx":
      outfile.write("\t"+ str(hsp.frame[0]))
    elif program == "tblastn":
      outfile.write("\t"+ str(hsp.frame[0]))
    outfile.write("\n")



def count_gap_openings(seq):
  '''
    counts gap openings in the sequence
    any "-" preceded by somthing other than a "-" is considered a gap opening
  '''
  return(len(re.findall('[^-]-', seq)))



if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-t", default=None, help="fasta target database")
  parser.add_argument("-q", default=None, help="fasta query sequences")
  parser.add_argument("-o", default=None, help="output file name")
  parser.add_argument("--temp_dir", default="tmp", help="directory for temporary data")
  parser.add_argument('--evalue', type=float, default=0.1, help="minimum e-value for a homolog match")
  parser.add_argument('--bitscore', type=float, default=0, help="minimum bit score for a homolog match")
  parser.add_argument('--ident', type=float, default=None, help="minimum percent identity for a homolog match")
  parser.add_argument('--sim', type=float, default=None, help="minimum percent similarity for a homolog match")
  parser.add_argument("--target_type", default="nucl", choices=('prot', 'nucl'), help="Is the target database a 'prot'ein or 'nucl'eic acid database?")
  parser.add_argument("--query_type", default="prot", choices=('prot', 'nucl'), help="Are the query sequences 'prot'eins or 'nucl'eic acids?")
  parser.add_argument("--parse_seqids", action='store_true', default=False, help="If set, sets the parse_seqids switch when calling makeblastdb")
  parser.add_argument("--skip_makedb", action='store_true', default=False, help="If set, skips the step where a new Blast database is built from the target sequences")
  parser.add_argument("--skip_blast", action='store_true', default=False, help="If set, skips the step where a Blast search is run. A Blast XML file must be present in the expected location.")
  parser.add_argument("--word_size", type=int, default=None, help="sets the word size for the blast search default for protein alignments is 3, the default for nucleotide alignments is 7")
  parser.add_argument("--num_threads", type=int, default=1, help="sets the number of threads for blast to use. Default 1")
  
  args = parser.parse_args()

  #If no output file is specified, output to stdout.
  outfile = sys.stdout
  if args.o is not None:
    outfile = open(args.o, 'w')
  
  main(outfile, args)
  outfile.close()
