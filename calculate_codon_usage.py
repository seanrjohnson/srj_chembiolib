#! /usr/bin/python
'''
  Reads a Fasta file, tabulates the codon usage in frame +1, outputs a tab separated list of codon usage mostly compatible with the format used in the tsv export from:
  https://hive.biochemistry.gwu.edu/dna.cgi?cmd=refseq_processor&id=569942
'''

import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import re
from collections import OrderedDict
import os 

#TODO: KAZUSA format output
def main(input_fasta_handle, output_handle, args):
  counts = OrderedDict()
  good_seqs = 0
  bad_seqs = 0
  for n2 in "TCAG":
      for n1 in "TCAG":
          for n3 in "TCAG":
              counts[n1+n2+n3] = 0

  for seq in SeqIO.parse(input_fasta_handle, args.seq_type):
    seq_str = str(seq.seq)
    seq_str = seq_str.upper()
    seq_str.replace("U","T")
    if len(seq_str) % 3 != 0:
      print("Sequence length not divisible by 3 " + seq.description, file=sys.stderr)
      bad_seqs += 1
      continue
    if (re.search('[^GACT]',seq_str) is not None):
      print("Sequence contains non-GACTU characters " + seq.description, file=sys.stderr)
      bad_seqs += 1
      continue
    if (args.skip_non_atg and (seq_str[0:3] != "ATG")):
      print("Sequence doesn't start with ATG: " + seq.description, file=sys.stderr)
      bad_seqs += 1
      continue


    for i in range(0,len(seq_str),3):
      counts[seq_str[i:i+3]] += 1
    good_seqs += 1
  print("total codons counted: " + str(sum(counts.values())), file=sys.stderr)
  print("total sequences counted: " + str(good_seqs), file=sys.stderr)
  print("total sequences skipped: " + str(bad_seqs), file=sys.stderr)
  if (args.include_header):
    print("\t".join(["Taxid", "Species","Translation Table"] + counts.keys()), file=output_handle)
  print("\t".join([str(args.taxID), str(args.species), str(args.table)] + [str(x) for x in counts.values()]))




if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--seq_type", default="fasta", help="")
  parser.add_argument("-i", default=None)
  parser.add_argument("-o", default=None)
  parser.add_argument("--include_header", action='store_true', default=False, help="If specified, then the output will include a header row")
  parser.add_argument("--taxID", default=None, help="The NCBI taxid of the species", required=True)
  parser.add_argument("--species", default=None, help="The name of the species", required=True)
  parser.add_argument("--table", default=1, help="The translation table number")
  parser.add_argument("--skip_non_atg", action='store_true', default=False, help="If specified, sequences not starting with ATG are ignored")
  parser.add_argument("--print_stats", action='store_true', default=False, help="If specified, then the total number of sequences and codons counted are printed to stderr.")

  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  main(input_handle, output_handle, args)
