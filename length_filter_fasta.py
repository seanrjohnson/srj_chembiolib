#! /usr/bin/python
'''

'''
import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import sys

def subset_fasta(input_handle, output_handle, length_lb, length_ub, seq_type):
  if ((length_lb is None) or (length_lb < 0)):
    length_lb = 0
  if (length_ub is None):
    length_ub = sys.maxsize
  if ((length_ub < 1) or (length_ub < length_lb)):
    sys.exit("Invalid upper bound. Must be > 0, and > lower bound")

  for seq in SeqIO.parse(input_handle, seq_type):
    keep = True
    length = len(seq)

    if ((length >= length_lb) and (length <= length_ub)):
      SeqIO.write(seq, output_handle, seq_type)


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--seq_type", default="fasta", help="")
  parser.add_argument("-i", default=None)
  parser.add_argument("-o", default=None)
  parser.add_argument("--length_ub", type=int, default=None, help="discard sequences longer than this")
  parser.add_argument("--length_lb", type=int, default=None, help="discard sequences shorter than this")

  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  subset_fasta(input_handle, output_handle, args.length_lb, args.length_ub, args.seq_type)
