import unittest
import os
import sys

class TestMassBankImport(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        from . import extract_massbank as MassBank
        with open(sys.path[0]+"/test/BML01218.txt") as infile:
            cls.ms = MassBank.MassBank(infile)
    def runTest(self):
        self.assertEqual(self.ms.data['ACCESSION'][0], 'BML01218')
        self.assertEqual(self.ms.data['MS$FOCUSED_ION BASE_PEAK'][0], '136')
        self.assertEqual(self.ms.data['PK$PEAK'][2], '119.0358 39 14')

if __name__ == '__main__':
    unittest.main()