"""
  Finds features in a gff file within a specified distance of a query feature
  returns a tab separated table with two columns: feature and distance from center.
"""
from srj_chembiolib.parsers import parse_simple_list
from BCBio import GFF
import argparse
import bisect
import sys

class Feature:
  def __init__(self, contig, name, start, end):
    self.name = name
    self.contig = contig
    if (start < end):
      self.start = start
      self.end = end
    else:
      self.start = end
      self.end = start

class SeqFromGFF: #seq in this case means "contig" primary children are features with one of the annotations of interest, secondary children are features with the other annotation of interest
  def __init__(self):
    self.primary_children = list() #list of all squences in the GFF
    self.secondary_children = list()

  def add_primary(self, name, start, end):
    self.primary_children.append(Annotation(name,start,end))
  def add_secondary(self, name, start, end):
    self.secondary_children.append(Annotation(name,start,end))

  def find_matches(self, thresh):
    out = list()
    if (len(self.secondary_children) > 0):
      for p in self.primary_children:
        #closest = 0
        closest_dist = min([abs(p.start - self.secondary_children[0].end), abs(p.end - self.secondary_children[0].start)])
        #for (i, s) in enumerate(self.secondary_children):
        for s in self.secondary_children:
          dist = min([abs(p.start - s.end), abs(p.end - s.start)])
          if (dist < closest_dist):
            #closest = i
            closest_dist = dist
        if ((thresh is None) or (closest_dist <= thresh)):
          out.append((p.name, closest_dist))
    return out
    #TODO: report only the closest match

###TODO: allow some kind of modification to node names, or GFF fields to align them
def main(output_handle, args):
  recs = dict() #
  name_qual = args.name_qual.strip()
  
  #print(nodes)
  #print(others)
  threshold = args.threshold
  if threshold is not None:
    threshold = threshold*1000

  center = None #a Feature representing the search center
  sorted_recs = list()
  with open(args.gff) as gff:
    for rec in GFF.parse_simple(gff):
      if ((args.feature_type is None) or (rec['type'] == args.feature_type)):
        if name_qual in rec['quals']:
          sorted_recs.append(Feature(rec['rec_id'],rec['quals'][name_qual][0],*rec['location']))
          if (args.center == rec['quals'][name_qual][0]): #TODO: warn if more than one center found
            center = sorted_recs[len(sorted_recs)-1]
  
  sorted_recs.sort(key=lambda x: x.start)
  sorted_recs.sort(key=lambda x: x.contig)
  center_index = sorted_recs.index(center) #TODO: binary search may be faster

  out = set()
  i = center_index - 1
  left_thresh = center.start - threshold
  right_thresh = center.end + threshold
  #TODO: this is actually a complicated problem. Need to maybe build a tree, or sort in two different ways (by start and by end), or just go until the beginning of the contig...
  print("\t".join([center.name, center.contig, str(center.start), str(center.end)]) , file=output_handle)
  #Go left
  while ((i >= 0) and (sorted_recs[i].contig == center.contig)):
    #r = sorted_recs[i]
    #print("\t".join([r.name, r.contig, str(r.start), str(r.end)]) , file=output_handle)
    if ( (sorted_recs[i].end > left_thresh) or (sorted_recs[i].start > left_thresh) ):
      out.add(sorted_recs[i])
    i -= 1 
  #Go right
  i = center_index + 1
  while ((i < len(sorted_recs)) and (sorted_recs[i].contig == center.contig)):
    #r = sorted_recs[i]
    #print("\t".join([r.name, r.contig, str(r.start), str(r.end)]) , file=output_handle)
    if ( (sorted_recs[i].end < right_thresh) or (sorted_recs[i].start < right_thresh) ):
      out.add(sorted_recs[i])
    i += 1
  
  for o in out:
    print("\t".join([o.name, o.contig, str(o.start), str(o.end)]) , file=output_handle)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-o", default=None, help="")
  #parser.add_argument("-i", default=None, help="a fasta file, the first column of the output will be the names of the sequences before the first space.")
  parser.add_argument("--gff", type=str, default=None, required=True, help="a gff file to look in.")
  parser.add_argument("--center", type=str, default=None, required=True, help="name of feature to be used as the center in the search for nearby sequences")
  parser.add_argument("--threshold", type=int, default=None, required=True, help="(in kb) find annotations within this distance from either end of the center annotation")
  parser.add_argument("--feature_type", type=str, default=None, required=False, help="the feature type of the center and target annotations in the GFF file, if None, then all feature types will be considered (possibly redundantly)")
  parser.add_argument("--name_qual", type=str, default=None, required=False, help="the field in the gff annotation column where center should be sought, and which will be returned in the output.")

  args = parser.parse_args()

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  main(output_handle, args)
  output_handle.close()
