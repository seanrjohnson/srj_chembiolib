import argparse
import sys
from correlated_mutations import MaskedMSA

def main(input_handle, output_handle, args):
  msa = MaskedMSA(input_handle, seqtype=args.seqtype ,mask_threshold=2)
  positions = args.positions.split(",")
  positions = [int(x.strip()) for x in positions]
  msa_positions = [msa.unaligned_index_to_masked_msa_index(args.reference_sequence,x) for x in positions]

  for seq in msa.msa:
    for i in range(len(positions)):
      print("%s\t%s\t%s" % (seq.name,str(positions[i]), seq[msa_positions[i]]), file=output_handle)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-o", default=None)
  parser.add_argument("-i", default=None, help="a fasta file, the first column of the output will be the names of the sequences before the first space.")
  parser.add_argument("--reference_sequence", type=str, default=None, required=True, help="Name of the sequence from the MSA which the positions are relative to.")
  parser.add_argument("--positions", type=str, default=None, required=True, help="positions of the reference sequence to generate annotations for. Comma separated")
  parser.add_argument("--seqtype", type=str, default="fasta", required=False, help="type of msa input file")
  #category will be the reference position followed by the 
  #parser.add_argument("--category", type=str, default=None, required=True, help="the second column of the output will be this repeated.")
  #parser.add_argument("--value", type=str, default=None, required=True, help="the third column in the output will be this repeated.")



  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  main(input_handle, output_handle, args)