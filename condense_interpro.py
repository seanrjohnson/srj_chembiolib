#! /usr/bin/python
'''
  Converts an interproscan XML file to a simple tab delimited file with one query per line.
'''

import argparse
from Bio import SearchIO
#import re
import sys

def main(infile, outfile):
  '''
  '''

  interpro_results = SearchIO.parse(infile, "interproscan-xml")
  
  records = dict()

  for r in interpro_results:
    #print(dir(r))
    if r.id not in records:
      records[r.id] = set()
    for h in r:
      #print("id " + h.id)
      #print("desc " + h.description)
      #if ("accession" not in dir(h)):
        #print(h.attributes['Target'])
        #print(h.id)
      if (str(h.description) != "<unknown description>"):
        records[r.id].add(h.attributes['Target']+ ":" + h.id + " " + h.description)

  for (k,v) in records.items():
    outfile.write(k + "\t" + '; '.join(v) + "\n")


if __name__ == '__main__':
  ap = argparse.ArgumentParser()
  ap.add_argument('-i', default=None)
  ap.add_argument('-o', default=None)
  
  args = ap.parse_args()
  
  infile = sys.stdin
  outfile = sys.stdout
  
  if args.i is not None:
    infile = open(args.i, 'r')
  if args.o is not None:
    outfile = open(args.o, 'w')
    
  main(infile, outfile)
  infile.close()
  outfile.close()
