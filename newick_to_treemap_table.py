import pandas as pd
from ete3 import Tree
import argparse
import sys
import csv

def pad_column_names(columns):
  width = max([len(str(x)) for x in columns])
  out = list()
  for x in columns:
    x = str(x)
    out.append(('0' * (width - len(x))) + x)

  return out

def generate_splits(node, output_list, split_list):
  '''
      
  '''
  
  if (len(node.children) == 0):
    #this is a leaf node, so add it to the output
    me = dict()
    me["name"] = node.name
    #me["id"] = node._nid
    for (i,v) in enumerate(split_list):
      me[i] = v
    me[i] = node.name
    output_list.append(me)
  else:
    for (i,child) in enumerate(node.children):
      #generate_splits(child, output_list, list(split_list + [i]))
      generate_splits(child, output_list, list(split_list + [i]))


def main(infile, outfile):
  
  nwck = ""

  for line in infile:
    nwck += line

  t = Tree(nwck, quoted_node_names=True)
  for node in t.traverse():
    node.name = node.name.strip('\'').strip()
  
  hierarchy = list()
  generate_splits(t, hierarchy, list())
  df = pd.DataFrame(hierarchy).set_index('name',drop=True).fillna("") #.astype('int64')
  df.columns = pad_column_names(df.columns)
  cols = df.columns 
  df["size"] = ["1"] * len(df)
  cols = ["size"] + list(cols)
  df = df[cols]
  df.to_csv(outfile, sep="\t", quoting=csv.QUOTE_ALL)



if __name__ == '__main__':
  ap = argparse.ArgumentParser()
  ap.add_argument('-i', default=None)
  ap.add_argument('-o', default=None)
  
  args = ap.parse_args()
  
  infile = sys.stdin
  outfile = sys.stdout
  
  if args.i is not None:
    infile = open(args.i, 'r')
  if args.o is not None:
    outfile = open(args.o, 'w')
    
  main(infile, outfile)
  infile.close()
  outfile.close()
