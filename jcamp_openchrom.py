from collections import OrderedDict
import numpy as np
from scipy import signal

def _split_point(point, sep=","):
  '''
    point: string
    output: None if the string does not look like a data point. A 2-tuple where the first entry is the x coord, and the second entry is the y-coord of the data point, both as floats.
    
    example data point:" 699.5, 4"
  '''
  out = None
  x = None
  y = None
  try:
    parts = point.split(sep)
    if len(parts) != 2:
      return None
    x = float(parts[0].strip())
    y = float(parts[1].strip())
    out = (x,y)
  except:
    pass
  return out

class JCAMP:
  '''
    A class for reading the jcamp-dx files exported from OpenChrom for GC-MS datasets.
    
    Assumptions:
      Scans are listed in order from lowest to highest retention time.
      XYDATA is in the form (XY..XY)
  '''
  def __init__(self, file_path):
    state="HEADER";
    self.props = dict();
    self.scans = OrderedDict();
    scan_num=0;
    header_types={"ELECTRON_ENERGY":float, "ACCELERATING_VOLTAGE":float, "SCAN_RATE":float, "SCAN_DELAY":float}
    data_types={"SCAN_NUMBER":int, "RETENTION_TIME": float, "TIC": int, "NPOINTS":int}
    with open(file_path) as infile:
      for line in infile:
        if line[0:2] == "##":
          fields = [x.strip() for x in line[2:].split("=")]
          if state == "HEADER":
            if fields[0] != "SCAN_NUMBER":
              self.props[fields[0]] = fields[1]
            else: #fields[0] == "SCAN_NUMBER"
              state = "DATA"
          if state=="DATA":
            if fields[0] == "SCAN_NUMBER":
              scan_num=int(fields[1])
              self.scans[scan_num] = {"properties":dict(), "points":list()}
            self.scans[scan_num]["properties"][fields[0]] = fields[1]
        else:
          point = _split_point(line)
          if point:
            self.scans[scan_num]["points"].append(point)

    ### cast the header property values into the correct type ###
    for (prop,value) in self.props.items():
      if prop in header_types:
        self.props[prop] = header_types[prop](value)
    
    ### cast the datapoint property values into the correct type ###
    ### Also convert the x,y array into numpy arrays ###
    for scan in self.scans:
      for (prop,value) in self.scans[scan]["properties"].items():
        if prop in data_types:
          self.scans[scan]["properties"][prop] = data_types[prop](value)
      self.scans[scan]["points"] = np.sort(np.array(self.scans[scan]["points"], dtype=[("x","f8"),("y","f8")]), order="x")
    
  
  def tic(self, x_units="minutes"):
    '''
      Total Ion Chromatogram
      
      returns a two-tuple where [0] is a list of retention times, and [1] is a list of abundance sums.
      
      x_units can be "seconds" or "minutes"
    '''
    x=list()
    y=list()
    
    for scan in self.scans:
      x.append(self.scans[scan]["properties"]["RETENTION_TIME"])
      y.append(self.scans[scan]["properties"]["TIC"])

    if x_units == "minutes":
      x = [t/60.0 for t in x]
    elif x_units != "seconds":
      raise ValueError("x_units %s not recognized" % str(x_units))
    
    return (np.array(x),np.array(y))
    
    
  def bpi(self, x_units="minutes"):
    '''
      Base Peak Intensity
      returns a two-tuple where [0] is a list of retention times, and [1] is a list of abundances of the most abundant ion at every retention time.
    '''
    x=list()
    y=list()
    
    for scan in self.scans:
      x.append(self.scans[scan]["properties"]["RETENTION_TIME"])
      y.append(np.max(self.scans[scan]["points"]["y"]))
    
    if x_units == "minutes":
      x = [t/60.0 for t in x]
    elif x_units != "seconds":
      raise ValueError("x_units %s not recognized" % str(x_units))
    
    return (np.array(x),np.array(y))
    
    
  def eic(self, ion, tolerance=0.5, tolerance_unit="amu", x_units="minutes", style="base"):
    '''
      Extracted Ion Current
      
      generates a chromatogram (a set of (x,y) coordinates) from a subset of the ions from each scan.
      
      tolerance_unit can be "amu" or "ppm"
      
      style can be "base" or "sum". If it is "base", then the y value returned will be the y-value for a scan will be the most abundant ion within the tolerance
        if style="sum" then the y-value for a scan will be the sum of all of the ion abundances within the tolerance.
      
    '''
    
    x=list()
    y=list()
    
    if tolerance < 0:
      raise ValueError("tolerance %s must be 0 or a positive number" % str(tolerance))
    if ion <= 0:
      raise ValueError("ion %s must be a positive number" % str(ion))
    if (tolerance_unit != "ppm") and (tolerance_unit != "amu"):
      raise ValueError("tolerance unit (%s) must be either ppm or amu" % str(tolerance_unit))
    if (style != "base") and (style != "sum"):
      raise ValueError("style (%s) must be either base or sum" % str(style))
    
    if (tolerance_unit == "ppm"):
      tolerance=(ion/1000000.0)*tolerance
    
    lb = ion - tolerance
    ub = ion + tolerance
    
    
    for scan in self.scans:
      
      x.append(self.scans[scan]["properties"]["RETENTION_TIME"])
      #print(np.logical_and((self.scans[scan]["points"]["y"] >= lb),(self.scans[scan]["points"]["y"] <= ub)))
      subset=self.scans[scan]["points"][np.logical_and((self.scans[scan]["points"]["x"] >= lb),(self.scans[scan]["points"]["x"] <= ub))]
      if (len(subset) == 0):
        y.append(0.0)
      else:
        if style == "base":
          y.append(np.max(subset["y"]))
        else:
          y.append(np.sum(subset["y"]))
    
    if x_units == "minutes":
      x = [t/60.0 for t in x]
    elif x_units != "seconds":
      raise ValueError("x_units %s not recognized" % str(x_units))
    
    return (np.array(x),np.array(y))
    
  def spectrum(self, at, x_units="minutes"):
    '''
      returns a two-tuple of x-y coordinates where the x-coordinate is an m/z, and the y-coordinate is an ion abundance.
      
      at: the coordinate of the desired mass spectrum. If the exact coordinate given is not found, then the data point at the coordinate closest to the requested coordinate will be returned
      unit: the units of "at", can be "seconds", "minutes", or "scan"
      TODO: implement exact indexing
    '''
    if x_units not in {"minutes","scan","seconds"} :
      raise ValueError("x_units (%s) must be minutes, scan, or seconds." % str(x_units))
    
    
    if x_units=="scan":
      scans = np.array([q["properties"]["SCAN_NUMBER"] for q in self.scans.values()])
    elif x_units=="minutes":
      scans = np.array([q["properties"]["RETENTION_TIME"]/60.0 for q in self.scans.values()])
    elif x_units=="seconds":
      scans = np.array([q["properties"]["RETENTION_TIME"] for q in self.scans.values()])
    idx = (np.abs(scans-at)).argmin()
    
    spectrum = self.scans[idx]["points"]
    
    return (spectrum["x"], spectrum["y"])

class AMDIS_MS:
  def __init__(self, file_path):
    
    self.compounds = OrderedDict();
    name=""
    
    with open(file_path) as infile:
      for line in infile:
        line = line.strip()
        if len(line) > 0:
          fields = line.split(":",1)
          if len(fields) == 2:
            if fields[0].upper() == "NAME":
              name = fields[1].strip()
              self.compounds[name] = {"points":list(), "NAME":name}
            else:
              self.compounds[name][fields[0].upper()] = fields[1].strip()
          # else:
            # print(line)
          elif len(fields) == 1:
            
            if ";" in line:
              
              line = line.strip(";")
              fields = line.split("; ")
              
            else:
              line = line.replace(") (", "\t")
              line = line.replace("(","")
              line = line.replace(")","")
              fields = line.split("\t")
            for point in fields:
              self.compounds[name]["points"].append(_split_point(point, None))
    for compound in self.compounds:
      self.compounds[compound]["points"] = np.sort(np.array(self.compounds[compound]["points"], dtype=[("x","f8"),("y","f8")]), order="x")
              
  def spectrum(self, compound):
    '''
      returns a two-tuple of x-y coordinates where the x-coordinate is an m/z, and the y-coordinate is an ion abundance.
    '''
    spectrum = self.compounds[compound]["points"]
    
    return (spectrum["x"], spectrum["y"])
            
    # state="HEADER";
    # self.props = dict();
    # self.scans = OrderedDict();
    # scan_num=0;
    # header_types={"ELECTRON_ENERGY":float, "ACCELERATING_VOLTAGE":float, "SCAN_RATE":float, "SCAN_DELAY":float}
    # data_types={"SCAN_NUMBER":int, "RETENTION_TIME": float, "TIC": int, "NPOINTS":int}
    # with open(file_path) as infile:
      # for line in infile:
        # if line[0:2] == "##":
          # fields = [x.strip() for x in line[2:].split("=")]
          # if state == "HEADER":
            # if fields[0] != "SCAN_NUMBER":
              # self.props[fields[0]] = fields[1]
            # else: #fields[0] == "SCAN_NUMBER"
              # state = "DATA"
          # if state=="DATA":
            # if fields[0] == "SCAN_NUMBER":
              # scan_num=int(fields[1])
              # self.scans[scan_num] = {"properties":dict(), "points":list()}
            # self.scans[scan_num]["properties"][fields[0]] = fields[1]
        # else:
          # point = _split_point(line)
          # if point:
            # self.scans[scan_num]["points"].append(point)

    # ### cast the header property values into the correct type ###
    # for (prop,value) in self.props.items():
      # if prop in header_types:
        # self.props[prop] = header_types[prop](value)
    
    # ### cast the datapoint property values into the correct type ###
    # ### Also convert the x,y array into numpy arrays ###
    # for scan in self.scans:
      # for (prop,value) in self.scans[scan]["properties"].items():
        # if prop in data_types:
          # self.scans[scan]["properties"][prop] = data_types[prop](value)
      # self.scans[scan]["points"] = np.sort(np.array(self.scans[scan]["points"], dtype=[("x","f8"),("y","f8")]), order="x")

def detect_peaks(x,y, consolidation_window=5, y_thresh=2, wavelet=[2]):
    '''
        Detect peaks in mass spectra
        x: array of x coordinates
        y: array of y coordinates
        consolidation_window: when a peak is detected, shift it to the highest y within a window of this many x-units
        y_thresh: ignore any peaks with a y value smaller than this number (as a percent of the highest y-value in the dataset)
        wavelet: for mass spectra, [2] seems to work well.
    '''
    y = normalize_pts(y)
    ci = signal.find_peaks_cwt(y,wavelet)
    out = set()
    for i in ci:
        m_x = x[i]
        m_y = y[i]
        indexes = ((x > (m_x - consolidation_window)) & (x < (m_x + consolidation_window))).nonzero()[0]
        for j in indexes:
            if y[j] > m_y:
                m_x = x[j]
                m_y = y[j]
        if (m_y > y_thresh):
            out.add(m_x)
    return out

def normalize_pts(x, maximum=100):
  '''
    normalizes the numbers in x, so that the largest is equal to maximum, and the other values are linearly scaled accordingly.
    
  '''
  
  a = np.array(x)
  mn = np.min(a)
  mx = np.max(a)
  if (mx > mn): #prevent divide by zero
    a = a - mn #shifts everything down so min = 0 (a sort of baseline correction)
    mx = np.max(a)
  return (a/mx)*maximum
  
def plot_chromatogram(ax, x, y, normalize=False, x_range=[], y_shift=0, color="black", linewidth=0.4, label_x_ticks=True, label_y_ticks=True, tick_label_size=8 , alpha=1, smooth=False, alternative_maximum=None, smooth_params=(7,2)):
  '''
    draw a plot of a chromatogram on the supplied AxesSubplot
    
    ax: an axes object on which to draw the chromatogram
    x: array of x coordinates
    y: array of y coordinates
    normalize: if True, then set the highest y to 100 and scale the others proportionately
    x_range: sets the x-axis window of the plot. A 2-tuple.
    y_shift: a value to add to all y-values (after normalization if applicable)
    label_x_ticks: if True, then number labels will be displayed along the x-axis
    label_y_ticks: if True, then number labels will be displayed along the y-axis
    smooth: if set, then passes the y-coordinates through a quadratic Savitzky-Golay filter
    alternative_maximum: if set, and if normalize is set, then if the maximum datapoint in the dataset is below this 
  '''
  

  
  if ( (len(x_range) == 2) and (x_range[0] < x_range[1]) ):
    ix = np.logical_and(x >= x_range[0], x <= x_range[1])
    x = x[ix]
    y = y[ix]
  
  if smooth:
    y = signal.savgol_filter(y, *smooth_params, deriv=0)
  
  if normalize:
    if (alternative_maximum and (np.max(y) < alternative_maximum)):
      y = normalize_pts(y, (np.max(y)/alternative_maximum)*100.0)
    else:
      y = normalize_pts(y)
  
  
  y = y + y_shift
  
  ax.plot(x, y, linewidth=linewidth, color=color, alpha=alpha)
  
  if ( (len(x_range) == 2) and (x_range[0] < x_range[1]) ):
    ax.set_xlim(*x_range)
  
  if (not label_x_ticks):
    ax.set_xticklabels([])
    ax.set_xticks([])
  #ax.set_xticklabels(ax.get_xticklabels(), size=x_tick_label_size)
  for i in ax.get_xticklabels():
    i.set_fontsize(tick_label_size)
  if (not label_y_ticks):
    ax.set_yticks([])
  
  
  

  
def plot_mass_spectrum(ax, x, y, normalize=True, auto_detect_peaks=False, label_top_n=0, peak_label_separation=0, autodetect_peaks_parameters={"consolidation_window":5, "y_thresh":5,"wavelet":[2]}, label_closest_to=[], x_range=[], label_x_ticks=True, label_y_ticks=True, x_axis_label = None, y_axis_label = None, bar_width=0.4, negative=False, peak_label_size=8, tick_label_size=8):
  '''
    draw a plot of a mass spectrum on the supplied AxesSubplot
    
    ax: an AxesSubplot object on which to draw the chromatogram
    x: array of x coordinates
    y: array of y coordinates
    normalize: if True, then set the highest y to 100 and scale the others proportionately
    auto_detect_peaks: if True, then call detect_peaks to find high points and label them
    label_top_n: number of highest points to label
    peak_label_separation: modifies label_top_n behavior to ignore points if they are within this distance from a higher point
    label_closest_to: array of x-coordinates. Force labeling of all points that are the closest to any of the x-coordinates in this list.
    x_range: sets the x-axis window of the plot.
    label_x_ticks: if True, then number labels will be displayed along the x-axis
    label_y_ticks: if True, then number labels will be displayed along the y-axis
    bar_width: sets the width of the vertical lines of the plot
    
    TODO: implement the ability to draw peaks below the axis (the negative argument)
  '''
  pts_to_label = set() #
  y_top_margin = 0
  y_label_margin = 0
  
  if normalize:
      y = normalize_pts(y)
  
  #do this before sorting, because it will get all messed up if you wait until after sorting...
  detected_peaks = set()
  if auto_detect_peaks:
    detected_peaks = detect_peaks(x,y, **autodetect_peaks_parameters)
  
  #sort peaks by height
  y_sort=np.argsort(-1*y)
  y = y[y_sort]
  x = x[y_sort]
  
  y_top_margin = np.max(y) * 0.2
  y_label_margin = y_top_margin/2.5
  

  
  for l in set(label_closest_to).union(detected_peaks):
      pts_to_label.add(_find_nearest(x, l))
  

  
  if label_top_n > 0:
      i = 0
      for j in range(len(x)):
          if (_closest_dist(x[j],pts_to_label,x) > peak_label_separation):
              i += 1
              pts_to_label.add(j)
          if (i == label_top_n):
              break
  

  ax.bar(x,y, bar_width, color="black", snap=False)
  
  if (len(x_range) == 2):
      ax.set_xlim(*x_range)
  ax.set_ylim(0,np.max(y)+y_top_margin)
  
  if (not label_x_ticks):
      ax.set_xticklabels([])
      ax.set_xticks([])
  
  for i in ax.get_xticklabels():
    i.set_fontsize(tick_label_size)
  
  
  
  if (not label_y_ticks):
      ax.set_yticks([])
  else:
    for i in ax.get_yticklabels():
      i.set_fontsize(tick_label_size)
  
  if x_axis_label != None:
    ax.set_xlabel(x_axis_label,size=tick_label_size)
  
  if y_axis_label != None:
      ax.set_ylabel(y_axis_label,size=tick_label_size)
  
  for i in pts_to_label:
      x_coord = x[i]
      y_coord = y[i]
      ax.annotate(xy=(x_coord,y_coord), xytext=(x_coord,y_coord+y_label_margin), s=str(int(np.round(x_coord))), horizontalalignment='center', size=peak_label_size)

def _find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx

def _closest_dist(v,s,x):
    out = float("inf")
    for i in s:
        d = abs(v-x[i])
        if d < out:
            out = d
    return out

def ms_subtract(x1,y1,x2,y2, tol=0.11):
  '''
    Subtract the mass abundances of the second spectrum from those of the first spectrum
    
  '''
  out_y = np.copy(y1)
  #x2_index = {value: key for (key, value) in enumerate(x2)}
  for x_i in range(len(x1)):
    nearest_x2 = _find_nearest(x2, x1[x_i])
    if abs(x1[x_i] - x2[nearest_x2]) <= tol:
      
      # print(x1[x_i])
      # print (y1[x_i] - y2[nearest_x2])

      out_y[x_i] = out_y[x_i] - y2[nearest_x2]
      if out_y[x_i] < 0:
        out_y[x_i] = 0
  return (x1, out_y)