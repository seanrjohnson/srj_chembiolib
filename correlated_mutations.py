#! /usr/bin/python
'''
  Reads a multiple sequence alignment file. Outputs a ranked list of correlated mutations.
  
  Usage:
  

  TODO: not sure what the best way to handle gaps is. Right now just treating them as equals to amino acids.
'''
import sys
import argparse
import numpy as np
from Bio import AlignIO
import networkx as nx
import pandas as pd

#TODO: refactor to be object oriented, with an MSA class
# MSA class should have mask,and matrix, and matrix mask all abstracted away!

#class MSA:
# LEGAL_AA_CODES={c:i for i,c in enumerate("-ACDEFGHIKLMNOPQRSTUVWY")}
# AA_REVERSE_LOOKUP=len(LEGAL_AA_CODES)*[""]
# for i in LEGAL_AA_CODES:
#   AA_REVERSE_LOOKUP[LEGAL_AA_CODES[i]] = i
#  def __init__(self,):
#   self.msa
# def apply_threshold(self, threshold=0.7):
# pass
# def sanitize(self):
#  """remove sequences with unrecognized characters, report number of sequences removed"""
LEGAL_AA_CODES={c:i for i,c in enumerate("-ACDEFGHIKLMNOPQRSTUVWY")} #mapping characters to indexes
AA_REVERSE_LOOKUP=len(LEGAL_AA_CODES)*[""]
for i in LEGAL_AA_CODES:
  AA_REVERSE_LOOKUP[LEGAL_AA_CODES[i]] = i #mapping indexes to characters

def abs_F(xy,a,b):
  """
    calculates the abs(F(x,y,a,b)) function from eqn1 of:
    Kuipers, Remko K. P., Henk-Jan Joosten, Eugene Verwiel, Sjoerd Paans, Jasper Akerboom, John van der Oost, Nicole G. H. Leferink, Willem J. H. van Berkel, Gert Vriend, and Peter J. Schaap. “Correlated Mutation Analyses on Super-Family Alignments Reveal Functionally Important Residues.” Proteins: Structure, Function, and Bioinformatics 76, no. 3 (2009): 608–16. https://doi.org/10.1002/prot.22374.
  """
  n_xa = xy[a,:].sum()
  n_yb = xy[:,b].sum()
  n_xa_yb = xy[a,b]

  
  return np.absolute((n_yb)-(n_xa_yb/n_xa))

def cormut_correlation_theoretical_max(correlation_array):
  """
    If every amino acid were random, and two positions perfectly predicted each other, what would the cormut_correlation score be?
    Use this to normalize the scores from the real alignment, so they aren't tiny.
  """
  ca = correlation_array
  in_shape = ca.shape

  aa_options = in_shape[2]
  inverse_aa_options = 1.0/aa_options

  F_x_y_a_b = 1-(inverse_aa_options)

  N_x_a_and_y_b = inverse_aa_options

  return aa_options*F_x_y_a_b*N_x_a_and_y_b

def aa_frequency_matrix_pretty_print(x,y,cor_array):
  out = "\t" + "\t".join(AA_REVERSE_LOOKUP)
  
  for r in range(cor_array.shape[2]):
    out += "\n" + AA_REVERSE_LOOKUP[r] + "\t"
    out += "\t".join( ["%3f" % x*100 for x in cor_array[x,y,r,:]] )
  return out

def split_correlation_networks(G):
  """
    Input: a graph of correlation networks

    Output: a list of independent subgraphs of the input.
  """

  return list(nx.connected_component_subgraphs(G))

def phase_correlation_network():
  """
    
  """
  
  pass

def render_correlation_networks():
  """
    Draws a network plot of the correlation networks.
  """
  pass

def ndarray_to_index_value(ndarray, exclude_diagonal=True, sort=True):
  '''
    returns a list of tuples, where the values are tuples of indexes, and values
    if sort, sort by value highest to lowest
  '''

  in_shape = ndarray.shape
  flat = ndarray.flatten()
  out = list()

  if exclude_diagonal:
    for i in range(flat.size):
      shape = np.unravel_index(i ,in_shape)
      diag = (shape.count(shape[0]) == len(shape))
      if (not diag):
        out.append( (shape,flat[i]) )
  else:
    for i in range(flat.size):
      out.append( (np.unravel_index(i ,in_shape),flat[i]) )

  if sort:
    out = sorted(out, reverse = True, key = lambda x: x[1])

  return out

def render_msa_groups(msa, group1, group2, output_filename, correlation_networks=list(), colorblind_mode=False):
  """
    Creates a microarray heatmap-like rendering of the sequence where variants in group1 are green, the variants in group2 are red, and the variants present in both are yellow.
  """

  import cairo

  columns = msa.get_alignment_length() + 1 #extra column for amino acid names
  rows = len(LEGAL_AA_CODES) + 1 #extra row for column numbers
  cell_width = cell_height = 15
  #cell_width = cell_height * 2
  output_format = output_filename[-4:]
  group2_color = (0,1,0)
  if colorblind_mode:
    group2_color = (0,0,1)

  msa_1 = msa_to_frequencies(msa, description_filter=group1)
  msa_2 = msa_to_frequencies(msa, description_filter=group2)

  if (output_format == ".pdf"):
    surface = cairo.PDFSurface(output_filename, columns*cell_width, rows*cell_height)
  elif (output_format == ".png"):
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, columns*cell_width, rows*cell_height)
  else:
    raise ValueError("output_filename must end in either '.pdf' or '.png'")

  #with cairo.SVGSurface(svgio, 100, 100) as surface:
  context = cairo.Context(surface)
  ## first, draw the grid and write the AA names and column numbers
  context.set_line_width(0.25)
  ROW_LABELS = [""] + AA_REVERSE_LOOKUP

  context.set_operator(cairo.Operator.OVER)

  context.rectangle(0, 0 , (columns)*cell_width , (rows)*cell_height)
  context.set_source_rgba(1,1,1,1)
  context.fill()
  context.set_source_rgba(0,0,0,1)
  for i in range(1,rows+1):
    context.set_font_size(10)
    context.move_to(cell_width/4, i*cell_height-2)
    context.show_text(ROW_LABELS[i-1])
    context.move_to(0, i*cell_height)
    context.line_to(columns*cell_width, i*cell_height)
    context.stroke()
  for i in range(1,columns):
    context.set_font_size(5)
    context.move_to(i*cell_width, cell_height-2)
    context.show_text(str(i))
    context.move_to(i*cell_width, 0)
    context.line_to(i*cell_width, rows*cell_height)
    context.stroke()

  ## Now, draw the boxes, with the correct color!
  context.rectangle(cell_width, cell_height, (columns)*cell_width , (rows)*cell_height)
  context.set_source_rgba(0,0,0,1)
  context.fill()
  context.set_operator(cairo.Operator.LIGHTEN)
  for r in range(1,rows):
    for c in range(1,columns):
      intensity_1 = msa_1[r-1,c-1]
      context.rectangle(c*cell_width, r*cell_height , cell_width , cell_height)
      context.set_source_rgba(intensity_1,0,0,1)
      context.fill()
      intensity_2 = msa_2[r-1,c-1]
      context.rectangle(c*cell_width, r*cell_height , cell_width , cell_height)
      context.set_source_rgba(0,intensity_2,0,1)
      context.fill()
  context.set_operator(cairo.Operator.OVER)

  for i in range(1,rows):
    context.move_to(cell_width, i*cell_height)
    context.line_to(columns*cell_width, i*cell_height)
    context.set_source_rgba(1,1,1,1)
    context.stroke()
  for i in range(1,columns):
    context.move_to(i*cell_width, cell_height)
    context.set_source_rgba(1,1,1,1)
    context.line_to(i*cell_width, rows*cell_height)
    context.stroke()

  context.set_source_rgba(0,0,0,1)
  context.set_font_size(10)
  for r in range(1,rows+1):
    for c in range(1,columns):
      context.move_to((cell_width*c) + (cell_width/4), r*cell_height-2)
      context.show_text(ROW_LABELS[r-1])

  for G in correlation_networks:
    for n in G.nodes:
      context.rectangle((n+1)*cell_width-1, 0, cell_width+2 , (cell_height*rows))
      context.set_source_rgba(0,0,0,0.3)
      context.fill()


  if (output_format == ".pdf"):
    surface.finish()
    #close(output_file)
  elif (output_format == ".png"):
    surface.write_to_png(output_filename)
    surface.finish()

def msa_to_frequencies(msa, description_filter=None):
  '''
    Converts a multiple sequence alignment (msa) into an array of dimensions number_of_legal_aa_characters x sequence_length
    where the values are in the range (0,1) and sum to 1 for each column

    description_filter can be used to select a subset, based on name, of the sequences in the input msa.
  '''
  seqs = list()

  for seq in range(len(msa)):
    if ( (description_filter is None) or (description_filter in msa[seq].description) ):
      seqs.append(str(msa[seq].seq))
  tr = LEGAL_AA_CODES
  out = np.zeros((len(LEGAL_AA_CODES), msa.get_alignment_length()))
  for seq in seqs:
    for pos in range(msa.get_alignment_length()):
      out[tr[seq[pos]],pos] += 1
  out = out / len(seqs)
  return out

class MaskedMSA:

  def __init__(self, msa_filename, mask_threshold=0.5, seqtype="fasta"):
    """
      for no masking, use mask_threshold >= 1

      NOTE: this function does not generate correlation arrays or calculate correlation scores.
    """
    self.cor_array = None
    self.correlation_scores = None
    self.normalized_correlation_scores = None
    
    self.msa = AlignIO.read(msa_filename, seqtype)
    self.msa = self.sanitize_msa()
    self.msa_matrix = self.msa_to_matrix()
    self.mask = self.generate_msa_mask(mask_threshold)
    self.masked_msa_matrix = self.mask_msa_matrix()
    self.masked_msa = self.mask_msa()

    self.cor_array = None
    self.cormut_scores = None
    self.mutual_information = None
    self.MIp = None #normalized mutual information

    #self.generate_correlation_array(self.msa_matrix)
    #self.cormut_correlation_scores(self.cor_array)


  def initialize_correlation_array(self, ignore_gaps=False):
    """
      calculates raw correlations between positions in an msa_array

      output: an array of dimensions (alignment_length, alignment_length, AA_CODES_length, AA_codes_length)
      where the indexes are: (first_position_index, second_position_index, first_position_AA, second_position_AA)
      and the values are the frequency of the associations (0-1)

      TODO: implement ignore_gaps
    """
    msa_matrix = self.masked_msa_matrix

    in_shape = msa_matrix.shape
    out = np.zeros((in_shape[1],in_shape[1],len(LEGAL_AA_CODES),len(LEGAL_AA_CODES)), dtype=np.uint32)
    
    for seq_number in range(in_shape[0]):
      print(seq_number)
      for aa1 in range(in_shape[1]):
        for aa2 in range(in_shape[1]):
          out[aa1, aa2, msa_matrix[seq_number,aa1], msa_matrix[seq_number,aa2]] += 1

    self.cor_array = out.astype(np.float64)/np.float64(in_shape[0])
    

  def initialize_cormut_correlation_scores(self, normalize=True):
    """
      given a correlation array as generated by generate_correlation_array, calculate a modified mutual information (based on Kuipers paper) between positions in an alignment
      
      input: a correlation array generated by generate_correlation_array
      
      output: an array of dimensions: alignment_length x alignment_length, where the values are modified mutual information between the two positions

      uses eqn1 from:
      Kuipers, Remko K. P., Henk-Jan Joosten, Eugene Verwiel, Sjoerd Paans, Jasper Akerboom, John van der Oost, Nicole G. H. Leferink, Willem J. H. van Berkel, Gert Vriend, and Peter J. Schaap. “Correlated Mutation Analyses on Super-Family Alignments Reveal Functionally Important Residues.” Proteins: Structure, Function, and Bioinformatics 76, no. 3 (2009): 608–16. https://doi.org/10.1002/prot.22374.
      CM(x,y) = sum_over_amino_acids_a(sum_over_amino_acids_b(abs(F(x,y,a,b)) * prob(y=b and x=a) / (num_amino_acids^2*N))

      NOTE: my numbers seem to be a lot smaller than the numbers in the paper, but in the correct order
      I'm either mis-interpretting their formula, or it's not a great formula. Some of the implications...
      Should explore other options.

    """
    if self.cor_array is None:
      self.initialize_correlation_array()
    correlation_array = self.cor_array
    ca = correlation_array
    in_shape = ca.shape
    out = np.zeros((in_shape[0],in_shape[0]), dtype=np.float64)
    inner_loop_sums = np.zeros( (in_shape[0], in_shape[0], in_shape[2], in_shape[2] ), dtype=np.float64)
    #num_symbols_squared = len(LEGAL_AA_CODES)**2

    #print(in_shape)
    #TODO:speed up by first calculating array of indexes of ca where the values aren't 0. To eliminate inner if.
    # use ravel_multi_index and related functions
    for pos1 in range(out.shape[0]):
      print(pos1)
      for pos2 in range(out.shape[0]):
        for aa1 in range(in_shape[2]):
          for aa2 in range(in_shape[2]):
            if (ca[pos1,pos2,aa1,aa2] != 0):
              inner_loop_sums[pos1,pos2,aa1,aa2] = ca[pos1,pos2,aa1,aa2] * abs_F(ca[pos1,pos2,:,:],aa1,aa2)/in_shape[2]
    out = inner_loop_sums.sum(axis=(2,3))

    #diagonal will always be highly correlated with itself, so set it to zero to get it out of the way.
    np.fill_diagonal(out, 0, wrap=False)
    if (normalize):
      #max_score = cormut_correlation_theoretical_max(correlation_array)
      out = out/np.max(out)

    self.cormut_scores = out


  def initialize_mutual_information(self):
    """
      given a correlation array as generated by generate_correlation_array, calculate mutual information between positions in an alignment
      
      input: a correlation array generated by generate_correlation_array
      
      output: an array of dimensions: alignment_length x alignment_length, where the values are mutual information between the two positions

      uses eqn 9.1 from Ashenberg and Laub,http://dx.doi.org/10.1016/B978-0-12-394292-0.00009-6
    """

    if self.cor_array is None:
      self.initialize_correlation_array()
    correlation_array = self.cor_array
    ca = correlation_array
    in_shape = ca.shape
    out = np.zeros((in_shape[0],in_shape[0]), dtype=np.float64)
    inner_loop_sums = np.zeros( (in_shape[0], in_shape[0], in_shape[2], in_shape[2] ), dtype=np.float64)
    
    aa_freqs = msa_to_frequencies(self.masked_msa)

    #num_symbols_squared = len(LEGAL_AA_CODES)**2

    #print(in_shape)
    #TODO:speed up by first calculating array of indexes of ca where the values aren't 0. To eliminate inner if.
    # use ravel_multi_index and related functions
    for pos1 in range(out.shape[0]):
      print(pos1)
      for pos2 in range(out.shape[0]):
        for aa1 in range(in_shape[2]):
          for aa2 in range(in_shape[2]):
            if (ca[pos1,pos2,aa1,aa2] != 0):
              inner_loop_sums[pos1,pos2,aa1,aa2] = ca[pos1,pos2,aa1,aa2] * np.log2( ca[pos1,pos2,aa1,aa2]/(aa_freqs[aa1,pos1] * aa_freqs[aa2,pos2]) )
    out = inner_loop_sums.sum(axis=(2,3))

    #diagonal will always be highly correlated with itself, so set it to zero to get it out of the way.
    np.fill_diagonal(out, 0, wrap=False)

    self.mutual_information = out

  def initialize_MIp(self):
    """
      given a mutual information array as generated by initialize_mutual_information, calculate MIp score between two positions
      
      input: a mutual information array generated by initialize_mutual_information
      
      output: an array of dimensions: alignment_length x alignment_length, where the values are MIp between the two positions

      uses eqn 9.2 from Ashenberg and Laub,http://dx.doi.org/10.1016/B978-0-12-394292-0.00009-6

    """
    if self.mutual_information is None:
      self.initialize_mutual_information()
    MI = self.mutual_information
    in_shape = MI.shape
    out = np.zeros((in_shape[0],in_shape[0]), dtype=np.float64)
    #inner_loop_sums = np.zeros( (in_shape[0], in_shape[0], in_shape[2], in_shape[2] ), dtype=np.float64)
    
    #aa_freqs = msa_to_frequencies(self.masked_msa)

    #num_symbols_squared = len(LEGAL_AA_CODES)**2

    #print(in_shape)
    #TODO:speed up by first calculating array of indexes of ca where the values aren't 0. To eliminate inner if.
    # use ravel_multi_index and related functions
    mean_MIs = self.mutual_information.sum(axis=0)/(self.mutual_information.shape[0]-1)
    mean_MI = np.sum(mean_MIs)/mean_MIs.size


    for pos1 in range(out.shape[0]):
      print(pos1)
      for pos2 in range(out.shape[0]):
        out[pos1,pos2] = MI[pos1,pos2] - (mean_MIs[pos1] * mean_MIs[pos2])/mean_MI

    #diagonal will always be highly correlated with itself, so set it to zero to get it out of the way.
    np.fill_diagonal(out, 0, wrap=False)
    
    # if (normalize):
    #   #max_score = cormut_correlation_theoretical_max(correlation_array)
    #   out = out/np.max(out)

    self.MIp = out


  def get_positions_sorted_by_highest_correlation_rank(self, correlation_type="MIp"):
    '''
      returns list of position pairs and correlation scores sorted by strongest correlations first
      
      correlation type can be "MIp", "MI", or "cormut"
    '''
    cor_array = None
    if correlation_type == "cormut":
      if (self.cormut_scores is None):
        self.initialize_cormut_correlation_scores()
      cor_array = self.cormut_scores
    elif correlation_type == "MI":
      if (self.mutual_information is None):
        self.initialize_mutual_information()
      cor_array = self.mutual_information
    elif correlation_type == "MIp": 
      if (self.MIp is None):
        self.initialize_MIp()
      cor_array = self.MIp
    else:
      return None
    
    sorted_position_array = ndarray_to_index_value(cor_array)
    seen = set()
    out = list()
    for ((a,b),score) in sorted_position_array:
      if a not in seen:
        out.append(a)
        seen.add(a)
      if b not in seen:
        out.append(b)
        seen.add(b)

    return out

  def find_correlation_networks(self, threshold):
    """
      Input: a correlation_array, alignment_length x alignment_length, where the values are correlation scores between the two positions.
      
      Output: a networkx graph where node names are position names, and weights are scores
      
      If the correlation array is not symmetrical, the highest of the two numbers will be taken as the correlation between the positions

      TODO: automatic thresholding?
    """
    if (self.cormut_scores is None):
      self.initialize_cormut_correlation_scores()
    correlation_array = self.cormut_scores
    G=nx.Graph()

    for i in range(correlation_array.shape[0]):
      for j in range(i+1,correlation_array.shape[1]):
        score = np.max([correlation_array[i,j], correlation_array[j,i]])
        if score > threshold:
          G.add_edge(i,j,weight=score)
    return G
  
  def msa_to_matrix(self):
    """converts an msa to a numerical array of dimensions num_sequences x alignment length"""
    msa = self.msa
    tr = LEGAL_AA_CODES
    out = np.zeros((len(msa), msa.get_alignment_length()), dtype=np.uint8)
    for i in range(len(msa)):
      for j in range(msa.get_alignment_length()):
        out[i,j] = tr[msa[i,j]]
    return out

  def generate_msa_mask(self, threshold=0.5):
    """
      input: an msa_matrix
             threshold: if more than this fraction of the sequences have a gap at a certain position,
             then the mask will contain a zero in that position, else it will contain a 1.
      output: a vector of the same length as the alignment, containing 0 where the alignment has gaps in a fraction of the sequences equal to at least threshold, else 1.
    """
    msa_matrix = self.msa_matrix
    gap_code = LEGAL_AA_CODES["-"]

    pre_mask = np.zeros(msa_matrix.shape)
    pre_mask[msa_matrix == gap_code] = 1

    gap_fraction = pre_mask.sum(axis=0)/pre_mask.shape[0]
    mask = np.zeros(gap_fraction.shape)
    mask[gap_fraction < threshold] = 1

    return mask

  def mask_msa(self):
    msa = self.msa
    mask = self.mask
    new_msa = AlignIO.MultipleSeqAlignment([])
    for i in range(len(msa)):
      desc = msa[i].description
      seq = str(msa[i].seq)
      seq = "".join([seq[i] for i,v in enumerate(mask) if v > 0])
      new_msa.add_sequence(desc,seq)
    return new_msa

  def mask_msa_matrix(self):
    """
      apply a previously calculated mask to the msa matrix

      output: an MSA matrix containing only the columns of the input where a fraction less threshold are gaps
    """
    msa_matrix = self.msa_matrix
    mask = self.mask
    return np.array(msa_matrix)[:,mask == 1]

  def sanitize_msa(self):
    """
      return msa containing only sequences from the input which consist only of valid AA codes
    """
    msa = self.msa
    new_msa = AlignIO.MultipleSeqAlignment([])
    legal_chars = set(LEGAL_AA_CODES.keys())
    for seq in msa:
      chars_in_seq = set(str(seq.seq))
      if len(chars_in_seq.difference(legal_chars)) == 0:
        new_msa.append(seq)
    return new_msa

  def get_seq(self, seqname, masked = False):
    found = None
    msa = self.msa
    if masked:
      msa = self.masked_msa
    #mask = self.mask
    
    for seq in msa:
      if (seqname in seq.description):
        if found is None:
          found = seq
        else:
          raise ValueError("seqname found in multiple descriptions")
    return found

  def masked_index_to_unaligned_index_translator(self, seqname, offset=0):
    '''
      returns an array where the index is the position in the masked_msa and the value is the position in the unaligned reference sequence
      offset: number to add to the position in the unaligned sequence before returning
      seqname: a string to be found in one of the descriptions of the sequences in msa
          if seqname is found in more than one of the sequences, an exception will be raised

      All positions are zero-aligned!! use "offset" to change this.
    '''

    #TODO:This seems to be horribly broken, I'm not sure why... 

    found = None
    msa = self.msa
    mask = self.mask
    
    found = self.get_seq(seqname)

    seq_pos = 0
    #msa_pos = 0
    out = list()
    for i,c in enumerate(found):
      if (mask[i] == 1):
        out.append(seq_pos + offset)
      if (c != '-'):
        seq_pos += 1
        #msa_pos += 1

    return out



  def masked_index_to_unaligned_index(self, seqname, masked_msa_index, offset=0):
    """
      converts an index from an MSA to an index from an unaligned sequence
      msa_index: position in the msa of the target residue (zero-indexed)
      

      returns:
        position in the unaligned sequence of the target residue (one-indexed)
    """



    index = masked_index_to_unaligned_index_translator(seqname,offset)

    if masked_msa_index < 0 or masked_msa_index >= len(index):
      raise ValueError("invalid msa_index")

    return index[masked_msa_index]

    # seq_pos = 0
    # msa_pos = 0
    # for i,c in enumerate(found):
    #   if (mask[i] == 1):
    #     if msa_pos == masked_msa_index:
    #       return seq_pos + 1 + offset
    #     if (c != '-'):
    #       seq_pos += 1
    #     msa_pos += 1

  # def index_to_unaligned_index(self, seqname, msa_index, offset=0):
  #   """
  #     converts an index from an MSA to an index from an unaligned sequence
  #     msa: a MultipleSeqAlignment object from Bio.AlignIO
  #     seqname: a string to be found in one of the descriptions of the sequences in msa
  #         if seqname is found in more than one of the sequences, an exception will be raised
  #     msa_index: position in the msa of the target residue (zero-indexed)
  #     offset: number to add to the position in the unaligned sequence before returning

  #     returns:
  #       position in the unaligned sequence of the target residue (one-indexed)
  #   """
  #   msa = self.msa
  #   #This just passes a mask of all 1s to the the masked_index_to_unaligned_index function
  #   return masked_index_to_unaligned_index(msa,seqname,msa_index, [1]*msa.get_alignment_length(), offset)


  def unaligned_index_to_masked_msa_index(self, seqname, unaligned_index, none_on_masked=False):
    """
      finds the corresponding position in a multiple sequence alignment, given a position from an unaligned sequence as the input.
      
      msa: a MultipleSeqAlignment object from Bio.AlignIO
      seqname: a string to be found in one of the descriptions of the sequences in msa
          if seqname is found in more than one of the sequences, an exception will be raised
      unaligned_index: position in the unaligned sequence of the target residue (one-indexed)
      mask: a vector of the same length as the sequence alignment, 0 means the position is excluded, 1 means that it is included
      none_on_masked: if true, then if the sought index falls into a masked region of the alignment, then return None

      returns:
        position in the msa of the target residue (zero-indexed)
    """
    msa = self.msa
    mask = self.mask
    found = None
    
    for seq in msa:
      if (seqname in seq.description):
        if found is None:
          found = seq
        else:
          raise ValueError("seqname found in multiple descriptions")
    
    if unaligned_index < 1:
      raise ValueError("invalid unaligned_index")
    if found is None:
      raise ValueError("seqname not found in msa")
    
    seq_pos = 0
    msa_pos = 0
    for i,c in enumerate(found):
      #if (mask[i] == 1):
      if (c != '-'):
        seq_pos += 1

      if (mask[i] == 1):
        msa_pos += 1
      
      if seq_pos == unaligned_index:
        if ((not mask[i]) and none_on_masked):
          return None
        else:
          return msa_pos - 1
    
    #we got to the end of the sequence without finding the unaligned_index, so it must be beyond the length of the sequence
    raise ValueError("invalid unaligned_index")
    
    #if msa_index < 0 or msa_index >= sum(mask):
    #  raise ValueError("invalid msa_index")
    #return masked_index_to_unaligned_index(msa,seqname,msa_index, [1]*msa.get_alignment_length(), offset)


  # def unaligned_index_to_msa_index(self, seqname, unaligned_index):
  #   """
  #     finds the corresponding position in a multiple sequence alignment, given a position from an unaligned sequence as the input.
      
  #     msa: a MultipleSeqAlignment object from Bio.AlignIO
  #     seqname: a string to be found in one of the descriptions of the sequences in msa
  #         if seqname is found in more than one of the sequences, an exception will be raised
  #     unaligned_index: position in the unaligned sequence of the target residue (one-indexed)

  #     returns:
  #       position in the msa of the target residue (zero-indexed)
  #   """
  #   #This just passes a mask of all 1s to the the unaligned_index_to_masked_msa_index function
  #   return self.unaligned_index_to_masked_msa_index(seqname,unaligned_index, [1]*msa.get_alignment_length())

# def ndarray_to_index_value(ndarray, exclude_diagonal=True):
#   '''
#     returns a list of tuples, where the values are tuples of indexes, and values
#   '''

#   in_shape = ndarray.shape
#   flat = ndarray.flatten()
#   out = list()

#   if exclude_diagonal:
#     for i in range(flat.size):
#       shape = np.unravel_index(i ,in_shape)
#       diag = (shape.count(shape[0]) == len(shape))
#       if (!diag):
#         out.append( (shape,flat[i]) )
#   else:
#     for i in range(flat.size):
#       out.append( (np.unravel_index(i ,in_shape),flat[i]) )
#   return out

# def process_msa(input_handle, output_handle, args):
#   msa = AlignIO.parse(input_handle, args.seq_type)
#   msa_matrix = msa_to_matrix(msa)
#   cor_array = generate_correlation_array(msa_matrix)
#   cormut_scores = cormut_correlation_scores(cor_array)
#   flattened_cormut_scores = ndarray_to_index_value(cormut)
#   flattened_cormut_scores = sorted(flattened_cormut_scores, reverse = True, key = lambda x: x[1])