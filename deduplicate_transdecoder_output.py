#!/usr/bin/python
'''
  This program reads the nucleotide and peptide fasta files written by transdecoder, and where multiple predictions are found, keeps only the longest. Also renames the sequences to their original name without all the transdecoder cruft.
'''
import argparse
from Bio import SeqIO

def rec_info(rec, generic):
  desc = rec.description.strip()
  if generic:
    id = rec.id.strip()
  else:
    #id = desc.split("::")[1]
    id = desc.split(".p")[0]
  length = len(rec)

  return(desc,id,length)


def main(nuc_in, pep_in, nuc_out, pep_out, args):
  seq_type="fasta"

  pep_seq_names=dict() #key is sequence ID (everything before the first whitespace), value is a 2-tuple where the first entry is the genemark gene number, and the second entry is the length.
  #nuc_seq_names=dict() #key is sequence ID (everything before the first whitespace), value is a 2-tuple where the first entry is the genemark gene number, and the second entry is the length.
  #pass one, store all sequence definitions, when a new sequence is encountered, if the ID is already in the dicts
  for rec in SeqIO.parse(pep_in, seq_type):
    (desc,id,length) = rec_info(rec, args.generic)
    if id not in pep_seq_names:
      pep_seq_names[id] = (desc,length)
    elif int(length) > pep_seq_names[id][1]:
      pep_seq_names[id] = (desc,length)

  #pass two
  pep_in.seek(0)
  for rec in SeqIO.parse(pep_in, seq_type):
    (desc,id,length) = rec_info(rec, args.generic)

    if (id in pep_seq_names) and (desc == pep_seq_names[id][0]):
      rec.description = id.strip()
      rec.id = id.strip()
      SeqIO.write(rec, pep_out, seq_type)

  for rec in SeqIO.parse(nuc_in, seq_type):
    (desc,id,length) = rec_info(rec, args.generic)
    if (id in pep_seq_names) and (desc == pep_seq_names[id][0]):
      rec.description = id.strip()
      rec.id = id.strip()
      SeqIO.write(rec, nuc_out, seq_type)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--ni", default=None, required=True, help="nucleotide input fasta")
  parser.add_argument("--pi", default=None, required=True, help="peptide input fasta")
  parser.add_argument("--no", default=None, required=True, help="nucleotide output fasta")
  parser.add_argument("--po", default=None, required=True, help="peptide output fasta")
  parser.add_argument("--generic", default=False, action='store_true', required=False, help="sequence names are not in Trinity format")

  args = parser.parse_args()

  with open(args.ni) as nuc_in:
    with open(args.pi) as pep_in:
      with open(args.no, "w") as nuc_out:
        with open(args.po, "w") as pep_out:
          main(nuc_in, pep_in, nuc_out, pep_out, args)
