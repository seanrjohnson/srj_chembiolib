#! /usr/bin/python
'''
  reads a fastq file containing both ends of a set of paired end reads. 
  writes two files, one for the left reads, one for the right reads
  
  expects sequences to be named by illumina conventions, with sequence
  names ending in "/1" for left side reads, and "/2" for right side reads
  
  Usage:
    split_paired_end_fastq.py side_1.fastq side_2.fastq combined.fastq
    split_paired_end_fastq.py side_1.fastq side_2.fastq < combined.fastq
    cat combined.fastq | split_paired_end_fastq.py side_1.fastq side_2.fastq
'''
import sys
import argparse


def iterate_fastq(in_handle):
  while True:
    name=in_handle.readline()
    if not name:
      break
    if (not name.startswith('@')):
        raise Exception('Malformed record, doesn\'t start with @ : %s' % name )
    else:
      name=name[1:].strip()
      seq=in_handle.readline().strip()
      name2=in_handle.readline().strip()
      quality=in_handle.readline().strip()
      yield(name, seq, name2, quality)



def split_fastq(in_handle, out_1_handle, out_2_handle):
  for lines in iterate_fastq(in_handle):
      if lines[0][-2:] == "/1":
        out_1_handle.write("@" + "\n".join(lines) + "\n")
      elif lines[0][-2:] == "/2":
        out_2_handle.write("@" + "\n".join(lines) + "\n")      
      else:
        raise Exception("cannot determine pair side for sequences name %s" % lines[0])

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("side_1_output")
  parser.add_argument("side_2_output")
  parser.add_argument("input", nargs='?', default=None)
  args = parser.parse_args()
  out_side_1 = open(args.side_1_output, "w")
  out_side_2 = open(args.side_2_output, "w")
  if args.input is not None:
    input_handle = open(args.input, "r")
  else:
    input_handle = sys.stdin
  
  split_fastq(input_handle, out_side_1, out_side_2)
  
  
