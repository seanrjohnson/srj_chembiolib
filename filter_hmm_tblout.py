from parsers import parse_hmmer
import argparse
import re
import sys
"""
input: an hmmer tblout file,
				a target or query name or accession (or a boolean combination of these, using &&, ||, and ~~, as and, or, and not operators)
				if --search_field or --expr are left blank, then return all return_field's where any line fulfills the evalue and score criteria.
output: all corresponding target or query names or accessions
"""

class BooleanEvaluator:
	_Identifier_syntax = '[A-Za-z0-9-_.\|# ]+'

	def __init__(self,logic_expr):
		"""
			logic_expr should have syntax:
			Indentifier := [A-Za-z0-9-.|# ]+ (Note: identifiers cannot contain || or &&, or ~~)
			Expr := Identifier | (Expr) | Expr && Expr | Expr || Expr | ~~Expr
		"""
		(self.parts_list, self.identifier_positions) = self.tokenize(logic_expr)

	@staticmethod
	def tokenize(string):
		"""
			converts a string into a list of tokens
		"""
		string=string.strip()
		parts = re.split('(&&|\|\||\~\~)', string)
		out_list = list()
		Identifiers = dict() #keys are identifiers, values are lists of positions in parts
		for (i , part) in enumerate(parts):
			if (part == '&&'):
				out_list.append(" and ")
			elif (part == "||"):
				out_list.append(" or ")
			elif (part == "~~"):
				out_list.append(" not ")
			else:
				id = part.strip()
				if (id == ""):
					pass
				else:
					id = id.strip("()").strip()
					if re.fullmatch(BooleanEvaluator._Identifier_syntax, id) is not None:
						if id not in Identifiers:
							Identifiers[id] = list()
						Identifiers[id].append(i)
						out_list.append(part.replace(id," 0 "))
					else:
						raise(ValueError("Badly formatted search values: " + str(parts)))
		if(string != ""):
			try:
				eval("".join(out_list))
			except:
				raise(ValueError("Badly formatted search values: " + str(parts)))

		return (out_list, Identifiers)

	def get_identifiers(self):
		return self.identifier_positions.keys()

	def eval(self, list_of_found_ids):
		expression_instance = list(self.parts_list)
		
		if (len(expression_instance) == 0):
			return True

		for id in list_of_found_ids:
			for pos in self.identifier_positions[id]:
				expression_instance[pos] = expression_instance[pos].replace(" 0 " , " 1 ")
		#print("".join(expression_instance))

		return eval("".join(expression_instance))


def main(input_handle, output_handle, args):
	evaluator = BooleanEvaluator(args.expr)
	targets = set(evaluator.get_identifiers())
	seen = dict()
	hmm = parse_hmmer(input_handle)
	if args.search_field is not None:
		search_field = args.search_field.replace("_"," ")
	else:
		search_field = None

	return_field = args.return_field.replace("_"," ")
	
	for m in hmm:
		if ( ((args.evalue is None) or (float(m['full sequence E-value']) <= args.evalue)) and ((args.score is None) or (float(m['full sequence score']) >= args.score)) ):
			ret = m[return_field].strip()
			if search_field is None:
				if ret not in seen:
					seen[ret] = set()
			else:
				search = m[search_field].strip()
				if search in targets:
					if ret not in seen:
						seen[ret] = set()
					seen[ret].add(search)

	for target in seen:
		if(evaluator.eval(seen[target])):
			print(target, file=output_handle)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-o", default=None, help="file to print list of results from search field to")
  parser.add_argument("-i", default=None, help="an hmmer tblout file")

  parser.add_argument("--search_field", type=str, default=None, required=False, choices={"target_name", "target_accession", "query_name", "query_accession", None}, help="field to look for search values in")
  parser.add_argument("--return_field", type=str, default=None, required=True, choices={"target_name", "target_accession", "query_name", "query_accession"}, help="field whose values to print to output if one of search_values found in search field")

  #parser.add_argument("--partial", action='store_true', default=False, help="if set, then allow identifiers in expr to match as substrings to the search field")

  parser.add_argument("--score", type=int, default=None, required=False, help="score cutoff")
  parser.add_argument('--evalue', type=float, default=None, required=False, help="e-value cutoff")

  parser.add_argument('--expr', type=str, required=False, default="", help='boolean expression to use to evaluate values in search field.')

  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin


  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  main(input_handle, output_handle, args)
  input_handle.close()
  output_handle.close()