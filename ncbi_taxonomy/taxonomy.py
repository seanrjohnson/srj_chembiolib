import pandas as pd
from os import path
from collections import defaultdict

class Taxonomy:

  def __init__(self, data_path):
    
    self.names_tab = pd.read_table(path.join(data_path,"names.dmp"),sep="\t\|\t?",header=None, names=("tax_id","name_txt","unique name","name class"), index_col=False,engine="python")
    self.merged_tab = pd.read_table(path.join(data_path,"merged.dmp"),sep="\t\|\t?", engine="python",header=None, names=("old_tax_id","new_tax_id"), index_col=False)
    self.citations_tab = pd.read_table(path.join(data_path,"citations.dmp"),sep="\t\|\t?", engine="python",header=None, names=("cit_id","cit_key","pubmed_id","medline_id",'url','text', 'tax_id_list'), index_col=False)
    self.divisions_tab = pd.read_table(path.join(data_path,"division.dmp"),sep="\t\|\t?", engine="python",header=None, names=("division_id","division_cde","pubmed_id","division_name",'comments'), index_col=False)
    self.nodes_tab = pd.read_table(path.join(data_path,"nodes.dmp"),sep="\t\|\t?", engine="python",header=None, names=("tax_id","parent_tax_id","rank","embl_code",'division_id', 'inherited_div_flag','genetic_code_id', 'inherited_GC_flag','mitochondrial_genetic_code_id','inherited_MGC_flag','GenBank_hidden_flag','hidden_subtree_root_flag','comments' ), index_col=False)
    self.nodes_tab.set_index("tax_id", inplace=True)
    self.nodes_tab["child_tax_ids"] = self.nodes_tab.index.map(lambda x: set())
    self.name_index = defaultdict(list) #keys are node names, values are lists of corresponding tax_ids, note that different tax_ids of the same level can have the same name. For example there are two genera with the name Perilla, one is a spider, the other a plant.
    self._add_children_to_nodes_tab()
#    self.ancestors_index = defaultdict(set) #keys are taxIDs, values are sets of taxIDs of ancestors 
#    self.descendants_index = defaultdict(set) #keys are taxIDs, values are sets of taxIDs of descendants
    self._seed_name_index()
  
  def _add_children_to_nodes_tab(self):
    #     out_list = list()
    # if tax_id in self.nodes_tab.index:
    #   tax = tax_id
    #   while self.nodes_tab.loc[tax].parent_tax_id != 1:
    #     if ()
    #     out_list.append(self.nodes_tab.loc[tax].parent_tax_id)
    #     tax = self.nodes_tab.loc[tax].parent_tax_id
    for (i,r) in self.nodes_tab.iterrows():
      if i != 1:
        self.nodes_tab.loc[r["parent_tax_id"]].child_tax_ids.add(i)

  def _seed_name_index(self):
    for (i,r) in self.names_tab.iterrows():
      self.name_index[r["name_txt"]].append(r["tax_id"])

    #   self.name_index[r["name_txt"]].append(r["tax_id"])



  def name_to_taxid(self, name, rank="species"):
    """
      Returns a list of TaxIDs from the database with the specified name and rank
      if rank = None, then any rank will do.
    """
    #db_specs = tax.nodes_tab[tax.nodes_tab["rank"] == rank]
    #TODO: implement if exact=False
    
    out = list()
    if name in self.name_index:
      hits = self.name_index[name]
      
      for hit in hits:
        if ((self.nodes_tab.loc[hit,"rank"] == rank) or (rank == None)):
          out.append(hit)
    return out
    
  def taxid_to_name(self, taxid):
    '''
      Return the scientific name associated with a given taxid
    '''
    #TODO: This is really slow, I bet there's a way to make it faster
    df = self.names_tab[(self.names_tab["tax_id"] == taxid) & (self.names_tab["name class"] == "scientific name")]
    out = None
    if len(df) == 1:
      out = df.iloc[0]["name_txt"]
    elif len(df) > 1:
      print("more than one scientific name for TaxID %d" % taxid)
      out = df.iloc[0]["name_txt"]
      #TODO replace with exception
    return out
    
    
  def lineage(self, tax_id):
    '''
      returns all parent nodes of tax_id (starting with tax_id)
    '''
    out_list = list()
    if tax_id in self.nodes_tab.index:
      out_list.append(tax_id)
      tax = tax_id
      while self.nodes_tab.loc[tax].parent_tax_id != 1:
        out_list.append(self.nodes_tab.loc[tax].parent_tax_id)
        tax = self.nodes_tab.loc[tax].parent_tax_id
    return out_list
  
  def ranks(self, lineage_list):
    '''
      lineage_list is a list of tax_ids such that no two have the same rank.
      returns a dict where keys are rank names, and values are the tax_id from
        lineage_list corresponding to the rank key.
    '''
    out = dict()
    for l in lineage_list:
      
      out[self.nodes_tab.loc[l, "rank"]] = l
      
    return out

  def get_rank(self, tax_id, rank_name):
    '''
      tax_id is any tax_id
      rank_name is any rank_name
      returns the tax_id of the ancestor node of tax_id with rank rank_name
      returns None if no ancestor has the requested rank_name
    '''
    line = self.lineage(tax_id)
    rd = self.ranks(line)
    if rank_name in rd:
      return rd[rank_name]
    else:
      return None

  def get_descendants(self, taxid, rank=None):
    out = set()
    def descend(tid):
      if ((tid != taxid) and ((rank is None) or (rank == self.nodes_tab.loc[tid]["rank"]))):
        out.add(tid)
      for c in self.nodes_tab.loc[tid]["child_tax_ids"]:
        descend(c)
    descend(taxid)
    return out


  def add_ranks_to_names_table(self):
    '''
      returns a copy of names_tab with columns added for various ranks
    '''
    out = self.names_tab.copy()
    out["rank"] = out["tax_id"].map(lambda x: self.nodes_tab.loc[x]['rank'])
    out["genus"] = "" #out["tax_id"].map(lambda x: self.get_rank(x, "genus"))
    out["family"] = "" #out["tax_id"].map(lambda x: self.get_rank(x, "family"))
    out["kingdom"] = "" #out["tax_id"].map(lambda x: self.get_rank(x, "kingdom"))
    out["superkingdom"] = "" #out["tax_id"].map(lambda x: self.get_rank(x, "superkingdom"))

    def descend(names_tab_out, node, superkingdom, kingdom, family, genus):
      if names_tab_out.loc[node]["rank"] == "superkingdom":
        superkingdom = names_tab_out.loc[node]["rank"]
      elif names_tab_out.loc[node]["rank"] == "kingdom":
        kingdom = names_tab_out.loc[node]["rank"]
      elif names_tab_out.loc[node]["rank"] == "family":
        family = names_tab_out.loc[node]["rank"]
      elif names_tab_out.loc[node]["rank"] == "genus":
        genus = names_tab_out.loc[node]["rank"]

      names_tab_out.loc[node]["superkingdom"] = superkingdom
      names_tab_out.loc[node]["kingdom"] = kingdom
      names_tab_out.loc[node]["family"] = family
      names_tab_out.loc[node]["genus"] = genus
      for c in self.nodes_tab.loc[node]["child_tax_ids"]:
        descend(names_tab_out, c, superkingdom, kingdom, family, genus)

    descend(out, 1, "", "", "", "")
    return out 
    

def full_name_to_genus_name(full_name):
  import re
  out = ""
  match = re.match("^([A-Z]\w+)", full_name)
  if match:
    out = match.group(1)
  return out

def full_name_to_binomial_name(full_name):
  import re
  match = re.match("^([A-Z]\w+) ([a-z.]+)", full_name)
  out = ""
  if match:
    if "." in match.group(2) or match.group(2) == "sp" or match.group(2) == "spp" or match.group(2) == "ssp":
      #species is ambiguous, so return only the genus
      out = match.group(1)
    else:
      out = match.group(1) + " " + match.group(2)
  return out
