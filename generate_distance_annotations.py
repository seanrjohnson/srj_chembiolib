"""
input: a list of nodes (sequence IDs) of interest
       a list of other nodes to calculate distance from the nodes of interest
       a gff file containing genomic coordinates for all the nodes
       name_qual: the annotation field in the gff file to compare to the node names
       category: the type of relation between the nodes and the other nodes
       threshold: ignore matches at a distance greater than this
"""

from srj_chembiolib.parsers import parse_simple_list
from BCBio import GFF
import argparse
import sys

class Annotation:
  def __init__(self, name, start, end):
    self.name = name
    if (start < end):
      self.start = start
      self.end = end
    else:
      self.start = end
      self.end = start

class SeqFromGFF: #seq in this case means "contig" primary children are features with one of the annotations of interest, secondary children are features with the other annotation of interest
  def __init__(self):
    self.primary_children = list() #list of all squences in the GFF
    self.secondary_children = list()

  def add_primary(self, name, start, end):
    self.primary_children.append(Annotation(name,start,end))
  def add_secondary(self, name, start, end):
    self.secondary_children.append(Annotation(name,start,end))

  def find_matches(self, thresh):
    out = list()
    if (len(self.secondary_children) > 0):
      for p in self.primary_children:
        #closest = 0
        closest_dist = min([abs(p.start - self.secondary_children[0].end), abs(p.end - self.secondary_children[0].start)])
        #for (i, s) in enumerate(self.secondary_children):
        for s in self.secondary_children:
          dist = min([abs(p.start - s.end), abs(p.end - s.start)])
          if (dist < closest_dist):
            #closest = i
            closest_dist = dist
        if ((thresh is None) or (closest_dist <= thresh)):
          out.append((p.name, closest_dist))
    return out
    #TODO: report only the closest match




###TODO: allow some kind of modification to node names, or GFF fields to align them
def main(output_handle, args):
  recs = dict() #
  name_qual = args.name_qual.strip()
  
  nodes = set(parse_simple_list(args.nodes))
  others = set(parse_simple_list(args.others))
  #print(nodes)
  #print(others)
  threshold = args.threshold
  if threshold is not None:
    threshold = threshold*1000

  with open(args.gff) as gff:
    for rec in GFF.parse_simple(gff):
      if name_qual in rec['quals']:
        
        prot_id = rec['quals'][name_qual][0]
        rec_id = rec['rec_id']
        
        if ((args.feature_type is None) or (rec['type'] == args.feature_type)):
          if prot_id in nodes:
            #print(rec)
            if rec_id not in recs:
              recs[rec_id] = SeqFromGFF()
            recs[rec_id].add_primary(prot_id, *rec['location'])
          
          if prot_id in others:
            if rec_id not in recs:
              recs[rec_id] = SeqFromGFF()
            recs[rec_id].add_secondary(prot_id, *rec['location'])
  overall_shortest_distances = dict()
  for contig in recs.values():
    for match in contig.find_matches(threshold):
      if match[0] not in overall_shortest_distances:
        overall_shortest_distances[match[0]] = match[1]
      else:
        overall_shortest_distances[match[0]] = min(overall_shortest_distances[match[0]],match[1])
  for (name, dist) in overall_shortest_distances.items():
    print(name + "\t" + args.category + "\t" + str(dist), file=output_handle)






if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-o", default=None, help="")
  #parser.add_argument("-i", default=None, help="a fasta file, the first column of the output will be the names of the sequences before the first space.")
  parser.add_argument("--gff", type=str, default=None, required=True, help="a gff file containing features with names matching the nodes and names matching the others.")
  parser.add_argument("--nodes", type=str, default=None, required=True, help="a file containing names of nodes to be annotated (one per line)")
  parser.add_argument("--others", type=str, default=None, required=True, help="a file containing names of sequences to find the distance of from the node sequences (one per line)")
  parser.add_argument("--threshold", type=int, default=None, required=False, help="(in kb) if the distance is longer than this, then don't write to the output")
  parser.add_argument("--feature_type", type=str, default=None, required=False, help="the feature type of interest in the GFF file, if None, then all feature types will be considered (possibly redundantly)")
  parser.add_argument("--name_qual", type=str, default=None, required=False, help="the field in the gff annotation column that should be compared to the node names")
  parser.add_argument("--category", type=str, default=None, required=True, help="the second column of the output will be this repeated.")

  args = parser.parse_args()

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  main(output_handle, args)
  output_handle.close()
