#! /usr/bin/python
'''
input: 

output: 

'''
import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import re
import parsers
import correlated_mutations
import numpy as np




def main(input_fasta_handle, output_handle, args):

  forbidden_positions = set()
  if (args.forbidden_positions is not None):
    for line in parsers.simple_parser(args.forbidden_positions):
      forbidden_positions.add(int(line) - 1)
  msa = correlated_mutations.MaskedMSA(input_fasta_handle, args.gap_cutoff, seqtype=args.seq_type)
  reference_sequence = {"name":None,"index":None}
  if (args.reference_sequence is None):
    reference_sequence["name"] = msa.msa[0].name
    reference_sequence["index"] = 0
  else:
    for (i,seq) in enumerate(msa.msa):
      if args.reference_sequence == seq.name:
        reference_sequence["name"] = args.reference_sequence
        reference_sequence["index"] = i
  if reference_sequence["name"] is None:
    raise Exception('Cannot find index sequence')
  reference_sequence["sequence"] = str(msa.msa[reference_sequence["index"]].seq).replace('-','') #gets the original sequence of the reference (without any masking)

  correlation_position_count = int(args.correlation_cutoff * len(reference_sequence["sequence"]))

  ref_to_msa = dict() #keys are 0-indexed positions
  msa_to_ref = dict() #keys are 0-indexed positions
  for i in range(0,len(reference_sequence["sequence"])):
    j = msa.unaligned_index_to_masked_msa_index(reference_sequence["name"],i+1,none_on_masked=True) #+1 to convert it to a traditional index, which is what that function takes...
    if j is not None:
      ref_to_msa[i] = j
      msa_to_ref[j] = i

  correlation_positions = list() #Positions that are too correlated to mutate. Will be converted to a set later.
  for j in msa.get_positions_sorted_by_highest_correlation_rank():
    if j in msa_to_ref: #it's not a gap in the reference sequence
      correlation_positions.append(msa_to_ref[j])
  correlation_positions = set(correlation_positions[0:correlation_position_count])

  frequency_array = correlated_mutations.msa_to_frequencies(msa.masked_msa)
  conservation_postions = set() #positions that are too conserved to mutate
  for j in set(np.argwhere(np.max(frequency_array,axis=0) > args.conservation_cutoff).flatten()):
    if j in msa_to_ref:
      conservation_postions.add(msa_to_ref[j])

  do_not_mutate = conservation_postions.union(correlation_positions).union(forbidden_positions)

  for position in sorted(ref_to_msa.keys()):
    msa_position = ref_to_msa[position]
    if position not in do_not_mutate:
      possible_mutations = np.argwhere(frequency_array[:,msa_position] > 0).flatten()
      mutation_chars = [correlated_mutations.AA_REVERSE_LOOKUP[i] for i in possible_mutations if correlated_mutations.AA_REVERSE_LOOKUP[i] != '-']
      if len(mutation_chars) > 1:
        print("\t".join([str(position+1),"".join(mutation_chars)]), file=output_handle)






  #baseseq = msa.msa





  # seqs = SeqIO.parse(input_fasta_handle, args.seq_type)
  # first_seq = next(seqs)

  # seqname = first_seq.name
  # base_sequence = str(first_seq.seq).upper()

  # graph = IceGraph(base_sequence, allowed_mutations)

  # (optimized_seq, path_weight) = graph.shortest_path()
  # print(">" + seqname + " " + str(path_weight), file = output_handle)
  # print(optimized_seq, file = output_handle)


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None, help="A fasta protein alignment file.")
  parser.add_argument("--seq_type", default="fasta", help="type of alignment file")
  parser.add_argument("-o", default=None)
  parser.add_argument("--forbidden_positions", default=None, help="a file listing positions in the base sequence that should not be mutated. 1 position per line. Indexed from 1.")
  parser.add_argument("--correlation_cutoff", default=0.3, type=float, help="After calculating correlated mutations, they'll be sorted by strength and then iterated through until this fraction of amino acids have been found. The found amino acids will be excluded from mutation.") #TODO:the default is arbitrary, I don't really know what a good value should be.
  parser.add_argument("--conservation_cutoff", default=0.4, type=float, help="If more than this fraction of sequences have the same amino acid at this position, then don't allow mutations here.") #TODO:the default is arbitrary, I don't really know what a good value should be.
  parser.add_argument("--gap_cutoff", default=0.3, type=float, help="Don't allow mutations at positions where more than this fraction of the sequences have gaps.") #TODO:the default is arbitrary, I don't really know what a good value should be.
  parser.add_argument("--reference_sequence", default=None, type=str, help="Name of the sequence to use as a basis for mutation positions.")
  args = parser.parse_args()
  #TODO: an argument to not allow substitutions with below some frequency. Or to only allow the most common k-substitutions.

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  main(input_handle, output_handle, args)
