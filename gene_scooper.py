#!/usr/bin/python
"""
  Reads two sequence files, a 'target' file and a 'query' file. Finds the closest homologs of 'query' in 'target'.
  Each sequence in target is assigned to at most one sequence in query.
  For example, if query is a set of protein sequences, and target is a set of RNAseq contigs, it will find transcripts
  encoding homologs of the query proteins.
  Programs from BLAST+ must be in the PATH for this script to work

  Output is a tab separated text file in BLAST+ outfmt6 format with a header.
"""
import sys
import os
import subprocess
import argparse
from Bio.Blast import NCBIXML
from Bio import SeqIO
from Bio import Seq
import re
import pandas as pd
from pprint import pprint


def generate_title_line(blast_type):
  #target will be added as the name of the index column.
  title_line = "query percent_identity percent_query_coverage alignment_length mismatches gap_opens query_start query_end target_start target_end e-value bit_score"
  if blast_type == "blastn":
    title_line += " query_strand"
    title_line += " target_strand"
  elif blast_type == "blastp":
    pass
  elif blast_type == "blastx":
    title_line += " query_frame"
  elif blast_type == "tblastn":
    title_line += " target_frame"
  title_line += " target_aligning_region"

  return title_line.split()#title_line.replace(" ","\t")

def load_fasta(filename):
  out = dict()
  with open(filename) as infile:
    for rec in SeqIO.parse(infile, "fasta"):
      #print(rec.description)
      out[rec.description.split()[0]] = str(rec.seq)
      #out[rec.name] = str(rec.seq)

  return out

def targetp():
  #"targetp -P -c"
  pass

def main(outfile, args):

  #make the target database
  target_basename = os.path.basename(args.t)
  target_database = os.path.join(args.temp_dir, target_basename)
  parse_seqids = ""
  if not args.skip_makedb:
    if args.parse_seqids:
      subprocess.call(["makeblastdb", "-max_file_sz", "2GB", "-hash_index","-parse_seqids","-dbtype", args.target_type, "-in", args.t, "-out", target_database])
    else:
      subprocess.call(["makeblastdb", "-max_file_sz", "2GB", "-hash_index","-dbtype", args.target_type, "-in", args.t, "-out", target_database])

  program = "blastn"
  word_size = 3
  max_target_seqs = 1000000

  if args.target_type == "prot":
    if args.query_type == "prot":
      program = "blastp"

      if args.word_size is not None:
        word_size = args.word_size
      else:
        word_size = 3
    elif args.query_type == "nucl":
      program = "blastx"

      if args.word_size is not None:
        word_size = args.word_size
      else:
        word_size = 3
  elif args.target_type == "nucl":
    if args.query_type == "prot":
      program = "tblastn"

      if args.word_size is not None:
        word_size = args.word_size
      else:
        word_size = 7
    elif args.query_type == "nucl":
      program = "blastn"

      if args.word_size is not None:
        word_size = args.word_size
      else:
        word_size = 7

  num_threads = args.num_threads
  if num_threads <= 0 :
    num_threads = 1

  blast_output = os.path.join(args.temp_dir, os.path.basename(args.q) + "_" + target_basename + ".xml")
  if not args.skip_blast:
    print(" ".join([program, "-db", target_database,"-max_target_seqs", str(max_target_seqs) ,"-word_size", str(word_size), "-num_threads", str(num_threads) ,"-evalue", str(args.evalue), "-query", args.q, "-out", blast_output, "-outfmt", str(5)]))
    subprocess.call([program, "-db", target_database,"-max_target_seqs", str(max_target_seqs) ,"-word_size", str(word_size), "-num_threads", str(num_threads) ,"-evalue", str(args.evalue), "-query", args.q, "-out", blast_output, "-outfmt", str(5)])

  target_matches = dict() #dict of lists of recs
  with open(blast_output, "r") as blast_xml_input:
    blast_records = NCBIXML.parse(blast_xml_input)

    #find matches that fit the filter criteria and sort them according to the database sequence
    for rec in blast_records:
      #NOTE: this assumes that rec.alignments[0].hsps[0] is the best matching hsp
      for i_align in range(len(rec.alignments)):
        percent_identity = 100 * float(rec.alignments[i_align].hsps[0].identities)/float(rec.query_length)
        percent_similarity = 100 * float(rec.alignments[i_align].hsps[0].positives)/float(rec.query_length)
        percent_query_coverage = 100 * abs((rec.alignments[i_align].hsps[0].query_end - rec.alignments[i_align].hsps[0].query_start)/rec.query_length)
        #print(percent_similarity)
        if ((args.evalue is None) or (rec.alignments[i_align].hsps[0].expect <= args.evalue)):
          if ((args.bitscore is None) or (rec.alignments[i_align].hsps[0].score >= args.bitscore)):
            if ((args.ident is None) or (percent_identity >= args.ident)):
              if ((args.sim is None) or (percent_similarity >= args.sim)):
                if ((args.coverage is None) or (percent_query_coverage >= args.coverage)):
                  target_name = rec.alignments[i_align].hit_def.split()[0]     ###IF IT ERRORS near line 187, try swapping these comments
                  #target_name = rec.alignments[i_align].hit_id
                  if target_name not in target_matches:
                    target_matches[target_name] = list()
                  target_matches[target_name].append({'query': rec.query, 'alignment': rec.alignments[i_align], "query_length":rec.query_length})

  #sort matches by blast score (this may not be the best criterion, I don't know)
  for targ in target_matches:
    target_matches[targ] = sorted(target_matches[targ], key=lambda x: x['alignment'].hsps[0].score, reverse=True)

  #load extra fasta files
  ref_seq_files = dict() #keys are the name to be given to the column where the sequences are put. Values are dicts where keys are sequence IDs and values are sequences.
  target_seqs = load_fasta(args.t)
  if args.reference_seqs:
    seq_paths = args.reference_seqs.split(",")
    seq_titles = ["reference_seq_"+str(x+1) for x in range(len(seq_paths))]
    if args.reference_seq_names:
      for (i,title) in enumerate(args.reference_seq_names.split(",")):
        if i < len(seq_titles) and title != "":
          seq_titles[i] = title
    for (i,seq_path) in enumerate(seq_paths):
      ref_seq_files[seq_titles[i]] = load_fasta(seq_path)


  ref_tables = list()

  if (args.reference_tables):
    ref_table_paths = args.reference_tables.split(",")
    reference_indexes = list()
    if args.reference_index:
      reference_indexes = args.reference_index.split(",")
    for (i,t_path) in enumerate(ref_table_paths):
      if (len(reference_indexes) > i) and (reference_indexes[i] != ""):
        ref_tables.append(pd.read_table(t_path,index_col=reference_indexes[i],dtype=object))
      else:
        ref_tables.append(pd.read_table(t_path,index_col=0,dtype=object))
  #write output:
  out_index = list() #list of row names
  out_data = list() #list of dicts where keys are column, and values are data
  title_line = generate_title_line(program)
  title_line = title_line + [k+"_seq" for k in ref_seq_files.keys()]
  for targ in target_matches:
    ddict = dict()

    query_name = target_matches[targ][0]['query']
    query_length = target_matches[targ][0]['query_length']
    hsp = target_matches[targ][0]['alignment'].hsps[0]
    percent_query_coverage = "%.2f" % (100 * abs((hsp.query_end - hsp.query_start)/query_length))
    percent_identity = "%.2f" % (100 * float(hsp.identities)/float(hsp.align_length))
    length = str(hsp.align_length)
    mismatch = str(hsp.align_length-(hsp.identities+hsp.gaps))
    gapopen = str(count_gap_openings(hsp.match) + count_gap_openings(hsp.sbjct))
    query_start = str(hsp.query_start)
    query_end = str(hsp.query_end)
    subject_start = str(hsp.sbjct_start)
    subject_end = str(hsp.sbjct_end)
    e_value = str(hsp.expect)
    bits = str(hsp.bits)
    tmp_start=hsp.sbjct_start-1
    tmp_end=hsp.sbjct_end-1
    if (tmp_start < tmp_end):
      #print(targ)
      aligning_region=target_seqs[targ][tmp_start:tmp_end+1]
    else:
      aligning_region=Seq.Seq(target_seqs[targ])[tmp_end:tmp_start].reverse_complement()


    #title_line = "query percent_identity alignment_length mismatches gap_opens query_start query_end target_start target_end e-value bit_score"
    ddict["query"] = query_name
    ddict["percent_identity"] = percent_identity
    ddict["percent_query_coverage"] = percent_query_coverage
    ddict["alignment_length"] = length
    ddict["mismatches"] = mismatch
    ddict["gap_opens"] = gapopen
    ddict["query_start"] = query_start
    ddict["query_end"] = query_end
    ddict["target_start"] = subject_start
    ddict["target_end"] = subject_end
    ddict["e-value"] = e_value
    ddict["bit_score"] = bits
    ddict["target_aligning_region"] = aligning_region

    for (k,v) in ref_seq_files.items():
      ddict[k+"_seq"] = v[targ]
    out_index.append(targ)
    out_data.append(ddict)
  out_df = pd.DataFrame(out_data, index=out_index, columns=title_line)
  out_df.index.name = "targ"


  '''
    append additional data here
  '''
  for (i,t) in enumerate(ref_tables):
    #print(t)
    out_df["transcript_name"] = out_df.index.map(lambda x: x.split(".")[0])
    out_df = out_df.join(t,rsuffix="_"+str(i), on="transcript_name")
  out_df.to_csv(outfile, sep="\t")



def count_gap_openings(seq):
  '''
    counts gap openings in the sequence
    any "-" preceded by somthing other than a "-" is considered a gap opening
  '''
  return(len(re.findall('[^-]-', seq)))



if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-t", default=None, help="fasta target database. This should be the database of unknown sequences.")
  parser.add_argument("-q", default=None, help="fasta query sequences. This should be the reference sequences.")
  parser.add_argument("-o", default=None, help="output file name")
  parser.add_argument("--temp_dir", default="tmp", help="directory for temporary data")
  parser.add_argument('--evalue', type=float, default=0.1, help="minimum e-value for a homolog match")
  parser.add_argument('--bitscore', type=float, default=0, help="minimum bit score for a homolog match")
  parser.add_argument('--ident', type=float, default=None, help="minimum percent identity for a homolog match")
  parser.add_argument('--sim', type=float, default=None, help="minimum percent similarity for a homolog match")
  parser.add_argument('--coverage', type=float, default=None, help="minimum percent coverage of the query sequence of the alignment")
  parser.add_argument("--target_type", default="nucl", choices=('prot', 'nucl'), help="Is the target database a 'prot'ein or 'nucl'eic acid database?")
  parser.add_argument("--query_type", default="prot", choices=('prot', 'nucl'), help="Are the query sequences 'prot'eins or 'nucl'eic acids?")
  parser.add_argument("--parse_seqids", action='store_true', default=False, help="If set, sets the parse_seqids switch when calling makeblastdb")
  parser.add_argument("--skip_makedb", action='store_true', default=False, help="If set, skips the step where a new Blast database is built from the target sequences")
  parser.add_argument("--skip_blast", action='store_true', default=False, help="If set, skips the step where a Blast search is run. A Blast XML file must be present in the expected location.")
  parser.add_argument("--word_size", type=int, default=None, help="sets the word size for the blast search default for protein alignments is 3, the default for nucleotide alignments is 7")
  parser.add_argument("--num_threads", type=int, default=1, help="sets the number of threads for blast to use. Default 1")
  parser.add_argument("--reference_seqs", default=None, help="additional sequences to include in the output. Sequences will be extracted by comparing the name before the first space to the name before the first space of the target_seq. Comma separated list of fasta file names.")
  parser.add_argument("--reference_seq_names", default=None, help="what to name the columns containing sequences from the --reference_seqs files. Must be empty, or have the same number of entries as --reference_tables.")
  parser.add_argument("--reference_tables", default=None, help="additional data to append to the output table, cross-referenced based on reference_index field. Can take multiple files separated by commas.")
  parser.add_argument("--reference_index", default=None, help="column of the reference table containing the sequence name. Must be empty, or have the same number of entries as --reference_tables.")

  args = parser.parse_args()
  #If no output file is specified, output to stdout.
  outfile = sys.stdout
  if args.o is not None:
    outfile = open(args.o, 'w')

  main(outfile, args)
  outfile.close()
