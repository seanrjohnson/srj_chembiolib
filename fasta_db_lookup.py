from Bio import SeqIO
from parsers import parse_simple_list
import sys
import argparse

def main(input_handle, output_handle, args):
  db = SeqIO.index_db(args.db, format="fasta")

  accs = parse_simple_list(input_handle)
  for acc in accs:
    if acc in db:
      r = db[acc]
      print(">%s\n%s" % (acc, str(r.seq)), file=output_handle)
    else:
      print("%s not in database" % acc, file=sys.stderr)


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  
  parser.add_argument("--db", type=str, default=None, required=True, help="path to sqlite3 database")
  parser.add_argument("-o", default=None, help="file to write output fasta file to, default=stdout")
  parser.add_argument("-i", default=None, help="file containing a list of seqids to look up in the , one per line, default=stdin")

  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin


  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  main(input_handle, output_handle, args)
  input_handle.close()
  output_handle.close()