import argparse
import sys

def main(input_handle, output_handle, args):
  prev_len = 0
  prev_name = None
  for line in input_handle:
    line = line.strip()
    
    if line[0] == ">":
      parts = line.split(None,1)
      name = parts[0][1:]
      if ((prev_name is not None) and (args.len)):
        print("%s\t%s\t%s" % (prev_name,"length", prev_len), file=output_handle)
      print("%s\t%s\t%s" % (name,args.category, args.value), file=output_handle)
      if (args.desc and (len(parts) == 2)):
        print("%s\t%s\t%s" % (name,"description", parts[1]), file=output_handle)

      prev_len = 0
      prev_name = name
    else:
      prev_len += len(line)
  if(args.len):
    print("%s\t%s\t%s" % (prev_name,"length", prev_len), file=output_handle)


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-o", default=None)
  parser.add_argument("-i", default=None, help="a fasta file, the first column of the output will be the names of the sequences before the first space.")
  parser.add_argument("--category", type=str, default=None, required=True, help="the second column of the output will be this repeated.")
  parser.add_argument("--value", type=str, default=None, required=True, help="the third column in the output will be this repeated.")
  parser.add_argument("--desc", action='store_true', default=False, help="If set, then the sequence description (the name after the first space) will be converted into an annotation line")
  parser.add_argument("--len", action='store_true', default=False, help="If set, then the sequence length will be converted into an annotation")



  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  main(input_handle, output_handle, args)
