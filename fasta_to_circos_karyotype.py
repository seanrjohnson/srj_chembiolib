#! /usr/bin/python
'''
  Reads a Fasta file, writes a karyotype file compatible with Circos



  Usage:
    fasta_to_circos_karyotype.py -i input.fasta --out_dir output_directory
    cat input.fasta | fasta_to_circos_karyotype.py --out_dir output_directory
'''
import sys
import argparse
from Bio import SeqIO
import os

def main(input_fasta_handle, output_handle):

  for seq in SeqIO.parse(input_fasta_handle, 'fasta'):
    print("chr - " + seq.id + " " + seq.id + " " + "0" + " " + str(len(seq)) + " " + "black", file=output_handle)



if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None)
  parser.add_argument("-o", default=None)

  args = parser.parse_args()

  if args.i is not None:
    input_handle=open(args.i, "r")
  else:
    input_handle = sys.stdin

  if args.o is not None:
   output_handle = open(args.o, "w")
  else:
   output_handle = sys.stdout

  main(input_handle, output_handle)
  input_handle.close()
  output_handle.close()
