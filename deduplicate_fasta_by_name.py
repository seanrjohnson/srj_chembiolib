#!/usr/bin/python
'''
  Reads a fasta file, write a fasta file without duplicate names
'''
import argparse
from Bio import SeqIO
import hashlib
import sys

# def rec_info(rec):
#   desc = rec.description.strip()
#   id = desc.split("::")[1]
#   length = len(rec)
  
#   return(desc,id,length)
  

def main(input, output):
  seq_type="fasta"
  
  seqs=dict() #key is sequence name, value is a hash of the sequence string
  
  
  
  for rec in SeqIO.parse(input, seq_type):
    #name = rec.name
    parts = rec.description.strip().split(None,1)
    #desc = ""
    name = rec.name
    #desc = rec.description
    if len(parts) > 1:
      desc = parts[1]
    
    #print(name)
    #print(desc)
    seq_hash = hashlib.md5(str(rec.seq).encode('utf-8')).hexdigest()
    #print(str(rec.seq))
    #print(str(seq_hash))
    iter = 0
    
    if name not in seqs:
      seqs[name] = seq_hash
      SeqIO.write(rec, output, seq_type)
    elif seq_hash != seqs[name]:
      print("duplicate with unique sequence " + name, file=sys.stderr)
      while name in seqs:
        iter += 1
        name = name + "_" + str(iter) + " " + desc
      rec.name = name
      rec.id = name
      rec.description = name + " " + desc
      print("\n"+name+"\n")
      #rec.description = name + "_" + str(iter) + " " + desc
      seqs[name] = seq_hash
      print(rec)
      SeqIO.write(rec, output, seq_type)
    else:
      print("duplicate! " + name, file=sys.stderr)
      pass

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None, required=True, help="input fasta")
  parser.add_argument("-o", default=None, required=True, help="output fasta")
  
  args = parser.parse_args()
  
  with open(args.i) as input:
    with open(args.o, "w") as output:
      main(input, output)
  input.close()
  output.close()


