#!/usr/bin/python
"""
  reads a tab separated data file where one of the columns is sequence name, and another is sequence. Writes a fasta file taking names from the name column and sequence from the sequence column.
"""
import sys
import pandas as pd
import argparse

def main(infile, outfile, args):
  
  df = pd.read_table(infile,index_col=None)
  for (i, r) in df.iterrows():
    print(">"+str(r[args.name_col])+"\n"+str(r[args.seq_col]),file=outfile)
    

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-o", default=None, help="output file name. If not supplied, output goes to stdout")
  parser.add_argument("-i", default=None, help="input file name. If not supplied, input comes from stdin")
  parser.add_argument("--seq_col", required=True, default=None, help="name of the column from the input to take the sequences from")
  parser.add_argument("--name_col", required=True, default=None, help="name of the column from the input to take the names from")
  
  
  args = parser.parse_args()
  
  infile = sys.stdout
  if args.i is not None:
    infile = open(args.i)
  
  outfile = sys.stdout
  if args.o is not None:
    outfile = open(args.o, 'w')

  main(infile, outfile, args)
  infile.close()
  outfile.close()