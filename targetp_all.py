#!/usr/bin/python
'''
  reads a fasta file of peptide sequences. Runs targetp on all of the sequences. Interface is the same as the original program but with a few additional options.
  Output is somewhat different from normal output from targetp: header information has been reduced to one row. The dash lines separating the sections are removed. The footer is removed. The sequence names are no longer truncated.
  Runs sequences in small batches to avoid errors associated with running targetp on large files.
'''
import sys
import os
import subprocess
import argparse
from Bio import SeqIO
import re
import io

def targetp(input, options, path="targetp", maximum_passes=3, _pass_number=1):
  '''
    calls targetp and returns a 2-tuple, at index 0 is a list of the fields from the output header, at index 1 is a list of lists of the data from the data lines: ([output header fields],[[data entries],])
  
    path:string pointing to full path of targetp executable (can be omitted if targetp is in the system path)
    input:fasta formatted string. If predictions come back as all zeros, consider breaking input down into smaller chunks.
    options: list of command line options for targetp, formatted with hyphens, like "-P", etc.
    
    The options: pass_number and maximum_passes are included to account for the random errors that sometimes occur when running targetp. If the function fails, it will call itself and try again, until the number of attempts equals maximum_passes, at which point it will print the error sequence names to stderr.
    
  '''
  
  
  call_list = [path] + options
  
  (names,seqs)=zip(*input)
  input_fasta=""
  for (i,name) in enumerate(names):
    input_fasta+=">"+str(i)+"_"+name+"\n"+seqs[i]+"\n"
  
  proc_call = subprocess.Popen(call_list, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  (stdout, stderr) = proc_call.communicate(input_fasta.encode("ascii"))
  stdout = str(stdout)
  out_header = ()
  out_data = list()
  lines = stdout.split("\\n")
  STATE="header" #header, data, footer
  div_str = "----------------------------------------------------------------------"

  
  for line in lines:
    line = line.strip()
    if (line == div_str):
      if STATE=="header":
        STATE="data"
      elif STATE=="data":
        STATE="footer"
    elif (STATE=="header") and (len(line) >=4) and (line[0:4] == "Name"):
      out_header = line.split()
    elif (STATE=="data"):
      out_data.append(line.split())
  if len(out_data) == len(names):
    for i,name in enumerate(names):
      out_data[i][0] = name
  else:
    if _pass_number < maximum_passes:
      targetp(input, options, path, maximum_passes, _pass_number+1)
    else:
      #print("Error with a targetp bin",file=sys.stderr)
      print("\n".join(names),file=sys.stderr)
      #print(out_data,file=sys.stderr)
    
  #out_data = list(zip(names,*list(zip(*out_data))[1:]))
  return (out_header,out_data)
  
  
def main(outfile, infile, args):
  
  targetp_exec ="targetp"
  if args.targetp_path:
    if os.path.isfile(args.targetp_path):
      targetp_exec = args.targetp_path
    else:
      raise ValueError("Invalid path for targetp")
  
  call_list = list()
  if args.P:
    call_list.append("-P")
  elif args.N:
    call_list.append("-N")
  if args.c:
    call_list.append("-c")
  if args.p:
    call_list.append("-p %f" % args.p)
  if args.s:
    call_list.append("-s %f" % args.s)
  if args.t:
    call_list.append("-t %f" % args.t)
  if args.o:
    call_list.append("-o %f" % args.o)

  
  def run_cache(cache):
    opts = {"input":cache,"options":call_list,"path":targetp_exec}
    (out_header, out_data) = targetp(**opts)
    
    if not run_cache.header_written:
      outfile.write("\t".join(out_header)+"\n")
      run_cache.header_written = True
    
    for data_line in out_data:
      outfile.write("\t".join(data_line)+"\n")
  run_cache.header_written = False
  
  cache = list() #list of 2-tuples where the first entry is the sequence name and the second is the aa sequence.
  num_seqs = 0
  for seq in SeqIO.parse(infile, 'fasta'):
    cache.append((str(seq.name),str(seq.seq)))
    num_seqs += 1
    if num_seqs == args.bin_size:
      run_cache(cache)
      cache=list()
      num_seqs = 0
  if len(cache) > 0:
    run_cache(cache)
    

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="A wrapper for targetp for processing large fasta files. Outputs to stdout.")
  parser.add_argument('input', nargs='?', default=None, help='Input fasta file, if none given then read from stdin')
  group = parser.add_mutually_exclusive_group(required=True)
  group.add_argument("-P", action='store_true', default=False, help="use plant networks")
  group.add_argument("-N", action='store_true', default=False, help="use non-plant networks")
  parser.add_argument("-c", action='store_true', default=False, help="include cleavage site predictions")
  parser.add_argument('-p', type=float, default=0.00, help="chloroplast prediction cutoff, default 0.00")
  parser.add_argument('-s', type=float, default=0.00, help="secretory pathway prediction cutoff, default 0.00")
  parser.add_argument('-t', type=float, default=0.00, help="mitochondrial prediction cutoff, default 0.00")
  parser.add_argument('-o', type=float, default=0.00, help="'other location' prediction cutoff, default 0.00")
  parser.add_argument('--targetp_path', default=None, help="path to targetp executable, if not in system PATH")
  parser.add_argument('--bin_size', type=int, default=1000, help="number of sequences to submit to targetp at one time. If this number is too large, the program will fail. If it is too small, the program will run slowly. I find that numbers between 500 and 1500 usually work well, but it may depend on the sizes of the proteins you have.")
  
  
  args = parser.parse_args()

  if args.bin_size < 1:
    raise argparse.ArgumentTypeError("bin size must be an integer greater than or equal to 1")
  
  #If no output file is specified, output to stdout.
  outfile = sys.stdout
  
  infile = sys.stdin
  if args.input is not None:
    infile = open(args.input, 'rU')
  
  main(outfile, infile, args)
  outfile.close()
#TODO: seems like there is a bug with the "input" optional positional parameter. Piping in the input works fine though.