import re
from collections import OrderedDict
import sys
import pandas as pd
import json
import argparse
from parsers import parse_diamond,parse_diamond_iterator,parse_fasta,parse_clstr,parse_simple_list
import math
#from pyfaidx import Fasta

#TODO: see if layouts are faster using networkx, or some other library, than with cytoscape. Cytoscape seems to handle around 50k nodes fairly readily.
#TODO: add styling. such as coloring by source, or default transparency

class edge:
	def __init__(self, source, dest):
		self.source = source
		self.dest = dest
		self.props = dict()

class node:
	def __init__(self, name):
		self.name = name
		self.all_names = set([name])
		self.x = 0
		self.y = 0
		self.props = dict()
	def add_syn(self, syn):
		self.all_names.add(syn)
	def add_prop(self, name, val): #could add support for different data types
		if name in self.props:
			if (type(self.props[name]) == set):
				self.props[name].add(val)
			elif (self.props[name] != val):
				self.props[name] = set([self.props[name]])
				self.props[name].add(val)
		else:
			self.props[name] = val
	def set_coord(self, x,y):
		self.x = x
		self.y = y



# def jsonify_string(s):
# 	return re.sub('[^A-Za-z0-9_$]','_',s)

def generate_ndex_cx(nodes, edges):
  	#TODO: add support for more properties
	jobj = list()
	node_objs = list()
	edge_objs = list()
	name_to_node_key = dict()
	name_to_edge_key = dict()
	number_verification = [{"longNumber": 0}] #I have no idea why this is necessary or what it means, the public documentation barely mentions it, but Cytoscape won't load any cx file that lacks it.
	cartesian_layout = list()
	node_attributes = list()
	edge_attributes = list()

	for (i, n) in enumerate(nodes.values()):
		new_node = dict()
		name_to_node_key[n.name] = i
		new_node["@id"] = i
		new_node["n"] = n.name
		node_objs.append(new_node)

		new_coord = dict()
		new_coord["node"] = i
		new_coord["x"] = n.x
		new_coord["y"] = n.y
		cartesian_layout.append(new_coord)

		accs = {"n": "Accessions", "po": i, 'd': "list_of_string", "v": list(n.all_names)}
		node_attributes.append(accs)
		#NOTE: all of the attributes of the same name "n: ...", must have the same type also. So for now, everything is just going to be lists of strings.
		for (k,v) in n.props.items():
			if (type(v) == set):
				prop = {"n": str(k), "po": i, "d": "list_of_string", "v": [str(x) for x in v]}
			else:
				prop = {"n": str(k), "po": i, "d": "list_of_string", "v": [str(v)]}
			node_attributes.append(prop)

	for (i, e) in enumerate(edges.values()):
		new_edge = dict()
		new_edge["@id"] = name_to_edge_key[e.source + "_to_" + e.dest] = i
		new_edge["s"] = name_to_node_key[e.source]
		new_edge["t"] = name_to_node_key[e.dest]
		edge_objs.append(new_edge)
		#e-value is a special case and is a double.
		#TODO:currently there is no handling of edge annotations other than e-values
		if "e-value" in e.props:
			ev = {"n": "e-value", "po": i, 'd': "double", "v": e.props["e-value"]}
			edge_attributes.append(ev)
		if "neg_log_evalue" in e.props:
			ev = {"n": "-log(e-value)", "po": i, 'd': "integer", "v": int(e.props["neg_log_evalue"])}
			edge_attributes.append(ev)




	jobj.append({"numberVerification": number_verification})
	jobj.append({"nodes": node_objs})
	jobj.append({"edges": edge_objs})
	jobj.append({"cartesianLayout": cartesian_layout})
	jobj.append({"nodeAttributes": node_attributes})
	jobj.append({"edgeAttributes": edge_attributes})


	return json.dumps(jobj)

def calc_neg_log_eval(num):
	if num < 1e-300:
		return 300
	elif num > 1:
		return 0
	else:
		return -1*math.log10(num)

def main(output_handle, args):
	subset=None
	if(args.subset is not None):
		subset=parse_simple_list(args.subset)


	if args.cdhit_clstr is not None:
		nodes = dict()
		seq_to_node = dict()
		clusters, member_to_cluster = parse_clstr(args.cdhit_clstr)
		for n in clusters.keys():
			if ( (subset is None) or (n in subset) ): #only make nodes for sequences that are listed in subset.
				nodes[n] = node(n) #keys are node names, values are node objects

		for n in member_to_cluster:
			if ( (subset is None) or (n in subset) ): #only make nodes for sequences that are listed in subset.
				seq_to_node[n] = member_to_cluster[n]
				nodes[seq_to_node[n]].add_syn(n)

	else:
		nodes = dict()
		seq_to_node = dict()
	
	edges = dict() #keys are tuples of node names, values are edge objects

	for rec in parse_diamond_iterator(args.all_to_all_diamond):
		#find mapping
		source = rec["qseqid"]
		dest = rec["sseqid"]
		if ( (subset is None) or ((source in subset) and (dest in subset)) ):
			if source not in seq_to_node:
				seq_to_node[source] = source
				nodes[source] = node(source)
			else:
				source = seq_to_node[source]
			if dest not in seq_to_node:
				seq_to_node[dest] = dest
				nodes[dest] = node(dest)
			else:
				dest = seq_to_node[dest]
			
			if (source != dest):
				if float(rec['evalue']) <= args.evalue:
					key = tuple(sorted((source,dest)))
					if (key not in edges):
						edges[key] = edge(*key)
						ev = float(rec['evalue'])
						if (args.evalue_annotations):
							edges[key].props["e-value"] = ev
						if(args.neg_log_evalue_annotations):
							edges[key].props["neg_log_evalue"] = calc_neg_log_eval(ev)
					elif (args.evalue_annotations and (ev < edges[key].props["e-value"])):
						edges[key].props["e-value"] = ev
						edges[key].props["neg_log_evalue"] = calc_neg_log_eval(ev)



	if args.annotation_table is not None:
		with open(args.annotation_table) as infile:
			for line in infile:
				line = line.strip()
				(seq_name, annot_type, annot) = line.split("\t",2)
				if (args.combine_annotations):
					annot=annot_type+":"+annot
					annot_type="annotation"
				if (seq_name in seq_to_node):
					nodes[seq_to_node[seq_name]].add_prop(annot_type,annot)
	
	if (args.fasta is not None):
		fasta = parse_fasta(args.fasta)
		for (n,s) in fasta.items():
			if (n in seq_to_node):
				nodes[seq_to_node[n]].add_prop("fasta",s)

	print("",file=sys.stderr)
	print("e-value: " + str(args.evalue),file=sys.stderr)
	print("Edge count: " + str(len(edges)),file=sys.stderr)
	print("Node count: " + str(len(nodes)),file=sys.stderr)
	print("Average node degree: %0.2f" % ( (len(edges)*2)/len(nodes) ) ,file=sys.stderr)
	print("",file=sys.stderr)
	print(generate_ndex_cx(nodes, edges), file=output_handle)





if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--all_to_all_diamond", type=str, default=None, required=True, help="a text table from Diamond (or blast outfmt6) from an all to all search of the nodes")
  parser.add_argument("--cdhit_clstr", type=str, default=None, required=False, help="a clstr file of the results of a cd-hit run on the network sequences")
  parser.add_argument("-o", default=None)
  parser.add_argument("--annotation_table", type=str, default=None, help="a 3-column tab separated table where the first column is a sequence name the second column is an annotation category, and the third column is an annotation value")
  parser.add_argument("--fasta", type=str, default=None, help="a fasta file with sequences corresponding to the sequences in the network. Sequences will be included as annotations in the graph")
  parser.add_argument("--subset", type=str, default=None, help="a file listing seqeuence names. Nodes will only be included in the network if they are listed in this file.")
  parser.add_argument('--evalue', type=float, required=True, help="e-value cutoff for generating an edge")
  parser.add_argument('--evalue_annotations', action='store_true', default=False, help="annotate each edge with the smallest e-value that covers it.")
  parser.add_argument('--neg_log_evalue_annotations', action='store_true', default=False, help="annotate each edge with the negative log10 of the smallest e-value that covers it. Range is truncated to [0-300]")
  parser.add_argument("--combine_annotations", action='store_true', default=False, help="If set, then all of the annotations are added to a single 'annotation' field in the form 'category:value'")

  args = parser.parse_args()

  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout

  main(output_handle, args)
